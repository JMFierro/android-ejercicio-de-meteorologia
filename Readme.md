## Ejercicio de App de Meteorologia ##

(Youtube: https://www.youtube.com/watch?v=0Tb8Xrpu9Tk)

***Jose Manuel Fierro Conchouso***

*U-Tad., Clase Android, 2013*

# Comentarios sobre el desarrollo #

1. **[ok]**	Diseño	general	de	la	interfaz	de	usuario	con	Actividades	y	Fragmentos,	utilizando	
correctamente	las	vistas	XML	y	los	menús.	(1,5	puntos)	
2. **[ok]**	Diferenciación	de	tablets	/	móviles.	(1	punto)	
3. **[ok]**	Uso	de	estilos	y	temas.	(1	punto)	
4. **[ok]**	Uso	de	navegación	efectiva	(lateral	y	ancestral 	especialmente)	(1	punto)	
5. **[ok]**	Carga	y	parseo	de	datos	mediante	llamadas	HTTP,	en	background.	(1,5	puntos)	>**clase WedServicio desde MainActivity**
6. **[ok]**	Carga	de	datos	sin	interacción	del	usuario	necesaria.	(1	punto)	
7. **[ok]**	Almacenamiento	persistente	de	la	información,	a	modo	de	caché,	en	base	de	datos	
SQLite.	(1	punto)	
8. **[ok]**	Gestión	correcta	de	preferencias	del	usuario.	(1	punto)	
9. **[ok]**	Perfecto	funcionamiento	de	la	app.	Por	“perfecta”	entenderemos	una	aplicación	que	
no	falla	nunca	y	controla	adecuadamente	todos	las	posibilidades	de	error.	(1	punto)	
	
Los	siguientes	extras	sumarán	a	la	nota	de	la	práctica:	

1. **[...]**	Aspecto	estético	de	la	aplicación.	
2. **[...]**	Utilidad	real	de	la	aplicación.	
3. **[ok]**	Cualquier	otra	funcionalidad	adicional	que	se	le	ocurra	al	alumno,	siempre	que	la	parte	principal	esté	cubierta. **(despliegue del item seleccionado)**


### APIS ###

Comienzo por la búsqueda de proveedores API de meteorología(servicios,datos,...).Me decido inicialmente por:

- **"Openweathermap"** que proporciona acceso a datos meteorológicos por idioma, utilizando tanto coordenadas como nombre de la ciudad. Tambien se puede obtener un listado de ciudades.

- **"Metwit"** del que obtengo los iconos (más atractivos que el resto de proveedores)


### Descripción app ###
La aplicación en un principio constara de tres pantallas:

- Pantalla 1: Lista de ciudades favoritas.
- Pantalla 2: Detalles de los datos meteorológicos de una ciudad elegida.
- Pantalla 3: Búsqueda y adicción de ciudades.

-> *en las pantallas de 7 pulgadas o más se muestra una vista diferenciada: el listado junto con el detalle a la derecha.*

##He puesto especial atención para que el paso entre pantallas (listado y detalle) sea solida y el listado se posicione mostrando el item seleccionado adecuado

En pantallas de menos de 7 pulgadas:


- Al volver al Listado desde la vista del Detalle, el item seleccionado en el listView será el mismo.

- Si se borra estando en la vista Detalle, se vuelve al listado y se posiciona en el item anterior al que se ha borrado.

- Si se hace navegacion lateral (viewPager) en la vista Detalle, al volver al listado este se situa en el último item que paracía en Detalle. 

- Al volver al listado se hace scroll si es necesario para que el listView este bien posicionado y el item seleccionado este visible. 

- ...etc

*Me llevó bastante tiempo las pruebas para que funcionará correctamente, sobre todo en la interface del movil.*

También me aseguré de que **después de que se actualice la base de datos, tanto la vista del Listado como el del Detalle se actualizan también**. La base de datos se actualiza mientras el usuario esta en cualquiera de las vistas, por lo que en ese momento puede no estar viendo los datos recientes. Cuando la base de datos termina de actualizarse, automáticamente se leen los datos y en ese momento se actualiza la vista. Mientras la base de datos se actualiza lo notifico en el titulo de la ActionBar *("...actualizandose la base de datos")*, y cuando termina restituyo el titulo original de la aplicación.

### Primeras actividades ###

Inicio el desarrollo con un **listView** que simula una lista de ciudades. Utilizo una **clase que hereda de Listfragment**.

La selección de una ciudad abre una **segunda actividad** que muestras los detalles meteorológicos de una ciudad.


### Diferenciación pantallas inferiores a 7 pulgadas ###

La vista sera diferente diferenciando pantallas de más de 7 pulgadas. En el fichero **"dimens.xml"** se inicializa la **variable "tablet"**, con *"false"* para pantallas pequeñas y con *"true"* para el resto, y la **variable "meteo\_main"** que tomara los valores *"main_unpanel" / "main\_dospaneles"* según proceda. Para ello sitúo *ficheros dimens.xml* con los valores adecuados en los directorios de *"res"* en *"values" y "values-sw600dp"*.


### Carga de datos ###

Al comienzo se inicia un servicio con **startSevice()** que avisa mediante un **broadcast** a la *"clase MainActivity"* cuando termina de hacer la descarga de datos y la actualización de la base de datos.

**Se  programa una alarma** para que  ejecute el servicio cada cierto periodo. Un Broadcast se registra en onCreate(). Se fija una alarma para que periódicamente se lean datos de internet. La periodicidad puede fijarse por el usuario desde las preferencias.

La descarga de datos se hace en un AsynTask, para que n o bloquee la UI del usuario.  


### PagerView ###

Para pantallas de menos de 7 pulgadas esta implementado un **pagerVier para la vista de Detalle**. Gestiono el cambio de posición, para poder borrar el adecuado y para posicionar el listView correctamente al volver a la Lista.


### Problema: eclipse no reconoce la tablet Nexus 7###

Tengo un portátil HP Pavilion dm3 con procesador Dual-Core (1,6 GHz), el mayor tiene una velocidad de 1,6 GHz y 4096 MB DDR2. No puede con el emulador virtual, a si que dependo de la tablet Nexus 7 para ejecutar el apk. 

Pues eclipse dejo de reconocerla, o mejor dicho, windows 7 la ignoraba por completo. ¡ Había estado funcionando bien desde el primer día hasta el ayer !. Buscando en internet y probando esto y aquello finalmente lo soluciono. 

HABIA QUE VOLVER A INSTALAR LOS DRIVES ORIGINALES DE NEXUS 7, ¡ Windows 7 por su cuenta y riesgo los reinstaló y puso los que le pareció !. 


### Menus ###

En pantallas menores a 7 pulgadas **diferencio los menús** de las vistas del Listado y Detalle. En Detalle no aparece la opción buscar(añadir localidad).  

La clase **BaseActionBarActivity()** para gestionar la visualización del menús por herencia. Empece a utilizar un método, **isActivityVisible()**, para saber que actividad es visible, guardando el hasdCode al pasar por **onResumen()**, de la actividad que se hacia visible.  En las clases que hereden deben de gestionar en **onResumen() y onPause()** los metodos activityResumed() y activityPaused().

Aunque funcionaba no me convencía. Busque en internet para averiguar como se sabia si una actividad era no visible desde otra clase. No encontré nada claro. Empece a probar por mi cuenta con hasdCode():

		int hasdMain = MeteoMainActivity.class.hashCode();
		int hasdDetalle = MeteoCiudadDetalle.class.hashCode();
		int hasdActual = getApplication().getClass().hashCode();
		int hasdClass = getClass().hashCode();
		int hasdthis = hashCode();

Al final con **gesClass().hashCode()** me da el "hasdCode" de la Actividad que esta visible:

		if (MeteoMainActivity.class.hashCode() == getClass().hashCode())
			getMenuInflater().inflate(R.menu.ciudades_lista, menu);
		if (MeteoCiudadDetalle.class.hashCode() == getClass().hashCode())
			getMenuInflater().inflate(R.menu.ciudad_detalle, menu);
 


### Fragmentos###

Creo un **fragmento estático** para que contenga el "listView" para las localidades. Llama a la clase **ListaLocalidadesFragmento()** que herada de **ListFragment**.

Utilizo un adaptador que puede ser reutilizado sin tener que rescribirlo, clase llamada "ListAdapterGenerico". Hereda de **BaseAdapter** y tiene un método abstracto **onMyItemView()** encargada de rellenar la vista "particular" del item en el lisView.

Se invoca desde **ListaLocalidadesFragmento()**, que es llamado directamente desde la vista **lista_localidades.xml**. Establezco llamadas  **callbacks** con la clase **MeteoMainActivity** a través del método **onMyItemViewSeleccionado()**. En *"ListaLocalidadesFragmento()"*, y mediante **onAttach()** me aseguro que se implementa y guardo una referencia para poder acceder a él.

Los fragmentos los hago compatibles con versiones anteriores a Android 3.0 ó nivel 11, utilizando **android.support.v4...**

Para la actividad del Detalle de la localidad uso un **fragmento dinámico**.


### Estilos y temas###

Establezco un **tema** para todas las actividades: **Theme.AppCompat.Light.DarkActionBar** compatible con versiones antiguas de android. Para implementarlo modifico los **dimmens.xml** en los diferentes directorios **values**. Hago que todos los textos tengan el color #A0A0A0 (gris) poniéndolo en el tema **AppTheme**, para que afecte a todas las actividades por defecto.

Para la actividad **MainActivity** cambio el tema por **LocalidadDetalleStyle** en el **AndroidManifiest.xml**. Pone el fondo negro. Hereda de AppTheme.

Pongo el **estilo** (ListViewItemStyle) en el **RelativeLayout** de **item\_lista\_localidades.xml**. De fondo para el item de la lista aplica un drawable  (background.9.png) y para el item presionado otro (list_item_selected.9.png). También fija margenes y padding.


### Search View en ActionBar y compatibilidad con API 7 ###

Para poder usar la introducción de texto desde *ActionBar* he utilizado **TextWatcher** y simulado un Search View. Tuve realizar numerosas pruebas y 
errores para una correcta simulación.

	        /*
			* Ocultar teclado
			* /
	        //et.setVisibility(View.INVISIBLE);
	        //et.setVisibility(View.GONE);
	        //et.requestFocus();
	        getWindow().setSoftInputMode(
	    	      WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	        //imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
	        isSearchView = false;
	        //int heigh = et.getHeight();
	        //et.setHeight(10);
			...
 
El resultado es mejorable pero tan sólo quería mostrar que se puede hacer.

Finalmente lo he desechado, no me convencía, sigo utlizando TexWatchwer para usar el método **afterTextChanged()** donde hago la lectura del editView esperando un retorno de carro("\\n") para finalizar. 


### Conexion a interner ###

Sin wifi en casa, simulo la lectura de datos desde un **fichero JSON que sitúo en res/raw**. Más adelante lo adapto a una conexión con internet.

Para detectar la localización del usuario lo hago a **traves de la red**.  El método **getLastKnownLocation()** me da la última ubicación en caché válida. Este método obtiene la ubicación en caché desde el proveedor de la red, necesita   esperar unos minutos a que tengas un punto válido. 


y con la clase **MyLocationListener** *implements LocationListener* escucha los cambios de localización.


	// Listener for managing location changes
	final class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {




### Eliminar localidad ###

Se elimina desde la opcion en el menú de ActionBar(X) y utilizo **AlertDialog** para pedir confirmación.

### Menu ###

En los menús he usado diferentes **showAsAction** para que, bien estén siempre presentes(búsqueda y settings) o bien sólo si hay sitio.

Esta implementado el **Menú lateral** con *DrawerToggle* en el icono home. Recoge pulsaciones en el menu y muestra un mensaje a modo de ejemplo.

### Notificaciones ###

Se envían notificaciones al concluir la descarga de datos meteorológicos de Internet y la actualización de la base de datos. También se informa si no hay conexión y si no se ha descargado.


### Añadir nueva localidad (startActivityForResult y Parcelable)###

Desde la **clase BaseActionBar** (buscar) se abre una nueva actividad con **startActivityForResult**, al escribir la localidad *se muestra opciones encontradas en un listview*, y tras seleccionar una por parte del usuario se añade a la lista de localidades.

De vuelta a la **clase BaseActionBar** recibe resultado en el método **onActivityResult()**, donde se lee el argumento parcelable (los datos de la localidad), que no utilizo ya que el elemento nuevo ya se añadió a la base de datos, pero envío el argumento para mostrar como hacerlo, a efectos del ejercicio.

Se puede cancelar la operación de añadir mediante un botón de cancel o con el "back", entonces se vuelve a la actividad anterior con **setResult(RESULT_CANCELED)**

Un EditText recibe la localidad, se busca en internet las opciones disponibles y las muestra en un listview para que el usuario seleccione una o pulse el boton de cancelar.

Si vuelve a tocar el Edit Text la actividad se reinicia con:

				/*
				 * Reinicia la actividad
				 */
				Intent intent = getIntent();
			    finish();
			    startActivity(intent);


### Actualizar lista de localidades tras eliminar un item ###

No podía refrescar el listview desde la clase *MenuActionBarActivity* cuando eliminaba un elemento tras pulsar la correspondiente opción de menú, debido a que estoy utilizando ListFragemt. Dándole vueltas, !muchas vueltas!, probando soluciones encontradas en internet, al final dí con la solución yo sólo. Guardar en una variable estática en la clase *ListLocalidadesFragement*  el contexto y desde este acceder a su un método de refresco del listview en la clase *MenuActionBarActivity*.

### Navegacion ##

 Esta implementado tanto la **navegacion ancestral** *(up en Action Bar)* como la lateral con **View Pager**. Ambos se pueden ver en la *vista Detalle* en pantallas para moviles (pantallas de < 7 pulgadas).

### Funcionalidad adiccional ###

Al pulsar un item de la lista **se despliega información adicional** sobre las predicciones semanales (datos ficticios a efectos del ejercicio).



### Resolución de iconos e imagenes ##

Lo ideal sería **vectorizar los iconos** y generar diferentes resoluciones para copiarlos en los **directorios *"drawable, drawable-xxhdpi"* de *"res"***. De esta forma en la *vista de Detalle*  no se perdería resolución al agrandar las imágenes.