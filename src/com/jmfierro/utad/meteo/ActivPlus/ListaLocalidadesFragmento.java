package com.jmfierro.utad.meteo.ActivPlus;

import java.util.ArrayList;


import com.jmfierro.utad.meteo.R;
import com.jmfierro.utad.meteo.Actividades.MainActivity;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Datos.DatosMeteoList;
import com.jmfierro.utad.meteo.Datos.Nota;
import com.jmfierro.utad.meteo.Utils.ListAdapterGenerico;
import com.jmfierro.utad.meteo.Utils.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ListaLocalidadesFragmento extends ListFragment { //implements MenuActionBarActivity.OnMenuListener {


	private final static String SAVED_INSTANCE_DATOSMETEOLIST = "DatosMdeteoList";
	private final static String SAVED_INSTANCE_ITEM = "Item";

	public static  ListaLocalidadesFragmento mListaLocalidadesFragThis;
	public static  DatosMeteoList mDatosMeteoList = new DatosMeteoList();
	public static  int mItemActual = 0;

	private ListView mListView;
	private Adapter mAdapter;



	/**============================================
	 * Callback para comunicar un elemento pulsado.
	 *=============================================*/
	private Callbacks mCallbacks = CallbacksVacios;


	public interface Callbacks {
		public void onMyItemViewSeleccionado(int posicion);
	}


	private static Callbacks CallbacksVacios = new Callbacks() {
		@Override
		public void onMyItemViewSeleccionado(int posicion) {

		}
	};


	/**==============================
	 * Necesario constructor vacio. 
	 *===============================*/
	public ListaLocalidadesFragmento() {
	}



	/**==============================
	 * Rellena cada item del listado.
	 *-------------------------------*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"onCreate()");


		if (savedInstanceState != null) {
			mDatosMeteoList = savedInstanceState.getParcelable(SAVED_INSTANCE_DATOSMETEOLIST);
			mItemActual = savedInstanceState.getInt(SAVED_INSTANCE_ITEM);

		}

		this.mListaLocalidadesFragThis = this;
	}


	@Override
	public void onSaveInstanceState(Bundle outState) {

		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"onSaveInstanceState()");

		if (mDatosMeteoList != null) {
			outState.putParcelable(SAVED_INSTANCE_DATOSMETEOLIST, mDatosMeteoList);
			outState.putInt(SAVED_INSTANCE_ITEM, mItemActual);
		}

		mListView = getListView();

		super.onSaveInstanceState(outState);
	}


	@Override
	public void onStart() { 
		super.onStart();

		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"onStart()");


//		if (getActivity().getApplication().getResources().getBoolean(R.bool.tablet))
		if (MainActivity.isTablet )   { //getResources().getBoolean(R.bool.tablet)) {

			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		}
		else {
			/*
			 * En principio en modo movil no se queda seleccionado el item,...
			 * 
			 * (aunque si despliego información extra en el item seleccionado,
			 * lo dejo asi porque me gusta)
			 */
			refrescarListaLocalidades();
			getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
		}

	}



	@Override
	public void onResume() {


		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"onResume()");

		/*
		 * Actualiza menu
		 */
		getActivity().supportInvalidateOptionsMenu();


		/*
		 * ..al final.
		 */
		super.onResume();


		/*
		 * Coloca el list en la posicion correcta.
		 */
		ListaLocalidadesFragmento.mListaLocalidadesFragThis
		.getListView().setSelection(mItemActual);

	}




	public void refrescarListaLocalidades() {

		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"refrescarListaLocalidades()");
		
		

		ArrayList<DatosMeteo> arrayDatosMeteoList = new ArrayList<DatosMeteo>();
		arrayDatosMeteoList = ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList();

		/*------------------------------
		 * Llama la adptador generico.
		 *------------------------------*/
		setListAdapter((new ListAdapterGenerico(getActivity(),
				R.layout.item_lista_localidades, ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList()){

			/*--------------------------------------------------------------------------
			 * Rellena un item para que lo use el adaptador ListAdapterGenerico.
			 *--------------------------------------------------------------------------*/
			@Override 
			public void onMyItemView(Object entrada, View view) {

				DatosMeteo datosMeteoEntrada  = (DatosMeteo)entrada;

				ImageView iconoPronostico = (ImageView) view.findViewById(R.id.img_Item_ListaLocalidades_Pronos);


//				if (ListaLocalidadesFragmento.this.mItemActual >= ListaLocalidadesFragmento.this.mDatosMeteoList.getSize()) {
//					ListaLocalidadesFragmento.this.mItemActual = ListaLocalidadesFragmento.this.mDatosMeteoList.getSize() -1;
//				}
//				
//				int i = ListaLocalidadesFragmento.this.mItemActual;

				int i = ListaLocalidadesFragmento.this.mItemActual;
				if (i >= ListaLocalidadesFragmento.this.mDatosMeteoList.getSize()) {
					i = ListaLocalidadesFragmento.this.mDatosMeteoList.getSize() -1;
				}

				long idSelect = ListaLocalidadesFragmento.this.mDatosMeteoList.getdatosMeteoList().get(i).getId();
				String nombreSelect = ListaLocalidadesFragmento.this.mDatosMeteoList.getdatosMeteoList().get(i).getNomLocalidad();
//				String nombreActual = datosMeteoEntrada.getNomLocalidad();

				long id = datosMeteoEntrada.getId();

				/*
				 * Imagen correspondiente al la meteorología
				 */
				String iconString = datosMeteoEntrada.getIcon(); 
				if (iconString.equals("01d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_01d));
				else if (iconString.equals("01n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_01n));
				else if (iconString.equals("02d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_02d));
				else if (iconString.equals("02n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_02n));
				else if (iconString.equals("03d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_03d));
				else if (iconString.equals("03n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_03n));
				else if (iconString.equals("04d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_04d));
				else if (iconString.equals("04n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_04n));
				else if (iconString.equals("09d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_09d));
				else if (iconString.equals("09n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_09n));
				else if (iconString.equals("10d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_10d));
				else if (iconString.equals("10n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_10n));
				else if (iconString.equals("11d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_11d));
				else if (iconString.equals("11n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_11n));
				else if (iconString.equals("13d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_13d));
				else if (iconString.equals("13n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_13n));
				else if (iconString.equals("50d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_50d));
				else if (iconString.equals("50n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_50n));
				else  
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_launcher));


				TextView textLocalidad = (TextView) view.findViewById(R.id.text_Item_ListaLocalidades_Nomb);
				textLocalidad.setText(datosMeteoEntrada.getNomLocalidad());
				TextView textPaisCiudad = (TextView) view.findViewById(R.id.text_Item_PaisCiudad_Nomb);
				textPaisCiudad.setText(datosMeteoEntrada.getNomPaisCiudad());

				TextView textLocalidadFecha = (TextView) view.findViewById(R.id.text_Item_ListaLocalidades_fecha);
				textLocalidadFecha.setText(datosMeteoEntrada.getFecha());
				if (Utils.getFechaActual().equals(datosMeteoEntrada.getFecha())){
//					textLocalidadFecha.setTextColor(Color.BLUE);
//					textLocalidadFecha.setTypeface(null, Typeface.BOLD);
				}
				else {
					textLocalidadFecha.setPaintFlags(textLocalidadFecha.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG );
				}

				// Pronostico de la semana
				TableLayout semanaTableLayout = (TableLayout) view.findViewById(R.id.item_Semana);
				if (idSelect != id) {
					semanaTableLayout.setVisibility(view.GONE); 
				}
				else {
					Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"refrescarListaLocalidades()...despliega item actual");

					//								view.setEnabled(true);
//					view.setBackgroundColor(Color.BLACK);
					if (MainActivity.isTablet)
						semanaTableLayout.setBackgroundColor(Color.BLACK);
					
//					textLocalidad.setBackgroundColor(Color.BLACK);
					textLocalidad.setTypeface(null, Typeface.BOLD);
					textLocalidad.setTextColor(Color.BLACK);
					textPaisCiudad.setTextColor(Color.RED);
					//								
//					view.setBackgroundResource(R.drawable.list_item_background);
					//								semanaTableLayout.setBackgroundResource(R.drawable.background);
					if (Utils.getFechaActual().equals(datosMeteoEntrada.getFecha())){
						textLocalidadFecha.setTextColor(Color.BLUE);
						textLocalidadFecha.setTypeface(null, Typeface.BOLD);
					}
				}
			}
		}));


		ListaLocalidadesFragmento.mListaLocalidadesFragThis
				.getListView().setSelection(ListaLocalidadesFragmento.mItemActual);
		
		


	}



	/**==============================================================================
	 * Asegura que este implementado el Callback en la clase que use a este Fragment.
	 *===============================================================================*/
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"onAttach()");

		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException("Error: La actividad debe implementar el callback del fragmento");
		}

		mCallbacks = (Callbacks) activity;
	}

	/**=================
	 * Limpia Callbacks.
	 *==================*/
	@Override
	public void onDetach() {
		super.onDetach();

		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"onDetach()");

		mCallbacks = CallbacksVacios; 
	}


	/*
	 * Cierra la base de datos
	 */
	@Override
	public void onPause() {
		//			dataSource.close();
		super.onPause();

		Log.d(ListaLocalidadesFragmento.class.getSimpleName(),"onPause()");
	}




	/**===============================================================
	 * Escucha la pulsación sobre un item de la lista. 
	 * Se usa el Callback para notificar a la Actividad el id pulsado.
	 *================================================================*/
	@Override
	public void onListItemClick(final ListView listView, View view, final int posicion, long id) {
		super.onListItemClick(listView, view, posicion, id);		

		listView.setItemChecked(posicion, true);
		mListView = listView;
		mAdapter = listView.getAdapter();
		mItemActual = posicion;

		// Pronostico de la semana
		TableLayout semanaTableLayout = (TableLayout) view.findViewById(R.id.item_Semana);
		if (MainActivity.isTablet )   { //getResources().getBoolean(R.bool.tablet)) {
			semanaTableLayout.setVisibility(view.VISIBLE);
		}

		mCallbacks.onMyItemViewSeleccionado(posicion);
	}

}
