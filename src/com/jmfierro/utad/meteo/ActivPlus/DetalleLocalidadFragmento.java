package com.jmfierro.utad.meteo.ActivPlus;

import com.jmfierro.utad.meteo.R;
import com.jmfierro.utad.meteo.Actividades.MainActivity;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Utils.Utils;

import android.R.layout;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class DetalleLocalidadFragmento extends Fragment {

	public static boolean isDetalleLocalidadFragmento = false;
//	public static boolean isNuevoItem = false;
	public static boolean isEliminarItem = false;
	
	public static final String EXTRA= "DatosMeteo";
	public static Activity mActivity = null;
	private DatosMeteo w;
	private String mTextTemp; 
	private DatosMeteo mDatosMeteoDetalle = new DatosMeteo(); 


	
	/**==============================================
	 * Constructor vacio para que pueda instanciarse 
	 *===============================================*/
	public DetalleLocalidadFragmento() {
	}

	
	
	/**=========================
	 * Recoge parámetros Bundle
	 *==========================*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub 
		super.onCreate(savedInstanceState);
		Log.d(DetalleLocalidadFragmento.class.getSimpleName(),"onCreate()");

		if (getArguments().containsKey(EXTRA)){
			mDatosMeteoDetalle = getArguments().getParcelable(EXTRA);
		}

		this.getActivity().setTitle(MainActivity.mMainActivityThis.getTitle());
		
	}


	
	/**=============================================
	 * Se crea la vista y se asocia con el layout.
	 * [en lugar de usar setContentView()]
	 *==============================================*/
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Log.d(DetalleLocalidadFragmento.class.getSimpleName(),"onCreateView()");
		
//		View viewFrag = inflater.inflate(R.layout.detalle_localidad_fragmento, container, false);
		View viewFrag;
		if (ListaLocalidadesFragmento.mDatosMeteoList.getSize() > 0) {
			viewFrag = inflater.inflate(R.layout.detalle_localidad_fragmento, container, false);
			
			if (mDatosMeteoDetalle != null) {

				// Datos meteorología de una localidad
				TextView textLocalidadNomb = (TextView) viewFrag.findViewById(R.id.text_LocalidadDetalle_Nomb);
				textLocalidadNomb.setText(mDatosMeteoDetalle.getNomLocalidad());
				TextView  textPaisNomb  = (TextView) viewFrag.findViewById(R.id.text_LocalidadDetalle_PaisNomb);
				textPaisNomb.setText(mDatosMeteoDetalle.getNomPaisCiudad());

				// Temperaturas
				TextView textTemp = (TextView) viewFrag.findViewById(R.id.text_LocalidadDetalle_Temp);
				textTemp.setText(Utils.quitarDecimales(this.mDatosMeteoDetalle.getTemp()) + "º");
				TextView textTempMaxMin = (TextView) viewFrag.findViewById(R.id.text_LocalidadDetalle_TempMaxMin);
				textTempMaxMin.setText(Utils.quitarDecimales(this.mDatosMeteoDetalle.getTemp_max()) + " - " 
						+ Utils.quitarDecimales(this.mDatosMeteoDetalle.getTemp_min()) + " ºC");
				TextView textFecha = (TextView) viewFrag.findViewById(R.id.text_LocalidadDetalle_Fecha);
				textFecha.setText(Utils.quitarDecimales(this.mDatosMeteoDetalle.getFecha()));

				if (Utils.getFechaActual().equals(this.mDatosMeteoDetalle.getFecha())){
					textFecha.setTextColor(Color.BLUE);
					textFecha.setTypeface(null, Typeface.BOLD);
				}
				else {
					textFecha.setPaintFlags(textFecha.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG );
				}

				// Varios
				TextView textDescr = (TextView) viewFrag.findViewById(R.id.textDescripcion);
				textDescr.setText((String)this.mDatosMeteoDetalle.getDescripcion());
				TextView textPresion = (TextView) viewFrag.findViewById(R.id.TextPresion);
				textPresion.setText((String)this.mDatosMeteoDetalle.getPresion() + " mb");
				TextView textHumedad = (TextView) viewFrag.findViewById(R.id.textHumedad);
				textHumedad.setText((String)this.mDatosMeteoDetalle.getHumedad() + " %");
				TextView textVelocidad = (TextView) viewFrag.findViewById(R.id.textVelocidad);
				textVelocidad.setText((String)this.mDatosMeteoDetalle.getVelocidad() + " Km/h");
				TextView textGrado = (TextView) viewFrag.findViewById(R.id.textGrado);
				textGrado.setText((String)this.mDatosMeteoDetalle.getGrado() + "->");


				/*
				 * Imagen correspondiente al la meteorología
				 */
				ImageView  iconoPronostico  = (ImageView) viewFrag.findViewById(R.id.img_LocalidadDetalle_Pronos);
				String iconString = this.mDatosMeteoDetalle.getIcon();
				if (iconString == null)
					iconString = "";
				
				if (iconString.equals("01d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_01d)); 
				else if (iconString.equals("01n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_01n));
				else if (iconString.equals("02d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_02d));
				else if (iconString.equals("02n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_02n));
				else if (iconString.equals("03d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_03d));
				else if (iconString.equals("03n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_03n));
				else if (iconString.equals("04d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_04d));
				else if (iconString.equals("04n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_04n));
				else if (iconString.equals("09d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_09d));
				else if (iconString.equals("09n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_09n));
				else if (iconString.equals("10d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_10d));
				else if (iconString.equals("10n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_10n));
				else if (iconString.equals("11d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_11d));
				else if (iconString.equals("11n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_11n));
				else if (iconString.equals("13d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_13d));
				else if (iconString.equals("13n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_13n));
				else if (iconString.equals("50d"))
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_50d));
				else if (iconString.equals("50n")) 
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.icon_weather_50n));
				else  
					iconoPronostico.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_launcher));


				TextView textCiudadPaisNomb = (TextView) viewFrag.findViewById(R.id.text_LocalidadDetalle_PaisNomb);
			}
		}

		else {
			viewFrag = inflater.inflate(R.layout.vista_void, container, false);
		}


		getActivity().supportInvalidateOptionsMenu();

		return viewFrag;
	}

	@Override
	public void onStart() {
		isDetalleLocalidadFragmento = true;
		mActivity = getActivity();

		super.onStart();
	}

	@Override
	public void onStop() {
		isDetalleLocalidadFragmento = false;
		mActivity = null;
		super.onStop();
	}
}





