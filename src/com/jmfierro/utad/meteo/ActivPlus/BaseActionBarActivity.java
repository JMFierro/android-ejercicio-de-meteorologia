package com.jmfierro.utad.meteo.ActivPlus;

import java.io.InputStream;


import com.jmfierro.utad.meteo.R;
import com.jmfierro.utad.meteo.Actividades.DetalleLocalidadActivity;
import com.jmfierro.utad.meteo.Actividades.MainActivity;
import com.jmfierro.utad.meteo.Actividades.NuevaLocalidadActivity;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Datos.DatosMeteoList;
import com.jmfierro.utad.meteo.Datos.Nota;
import com.jmfierro.utad.meteo.Preferencias.SettingsActivity;
import com.jmfierro.utad.meteo.SQL.NotasDataSource;
import com.jmfierro.utad.meteo.Utils.Utils;
import com.jmfierro.utad.meteo.Web.WebServicio;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

/*-------------------------------------------------------------
 * >> 	Gestiona el menu segun la actividad que esta visible.
 *-------------------------------------------------------------*/
//@SuppressLint("NewApi")
public abstract class BaseActionBarActivity extends ActionBarActivity implements TextWatcher {

	protected final static int CONFIRM_REQUEST_ID = 11;
	protected final static int REQUEST_CODE  = 1;
	private final static String SAVED_INSTANCE = "DatosMeteo"; 

	private EditText et;
	private String title;
	private boolean isSearchView;
	private InputMethodManager imm;
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		if (DetalleLocalidadActivity.class.hashCode() == getClass().hashCode()) {
			getMenuInflater().inflate(R.menu.detalle_localidad, menu);
			
			/*
			 * Deasctiva/activa opciones de menu dependiendo si esta actulizando 
			 * la base de datos.
			 */
			MenuItem eliminarDetalleMenuItem = menu.findItem(R.id.action_eliminar_detalle);
			if (WebServicio.isActualizandoseSQLfromWebTask) {
				if (ListaLocalidadesFragmento.mDatosMeteoList.getSize() > 0){
					eliminarDetalleMenuItem.setVisible(false);
				}
			}
			else {
				eliminarDetalleMenuItem.setVisible(true);
			}
		}
		
		else if (MainActivity.class.hashCode() == getClass().hashCode()) {
			getMenuInflater().inflate(R.menu.lista_localidades, menu);
			
			/*
			 * Deasctiva/activa opciones de menu dependiendo si esta actulizando 
			 * la base de datos.
			 */
			MenuItem eliminarMenuItem = menu.findItem(R.id.action_eliminar);
			MenuItem buscarMenuItem = menu.findItem(R.id.action_search);
			if (WebServicio.isActualizandoseSQLfromWebTask) {
				if (ListaLocalidadesFragmento.mDatosMeteoList.getSize() > 0){
					eliminarMenuItem.setVisible(false);
					buscarMenuItem.setVisible(false);
				}
			}
			else {
				eliminarMenuItem.setVisible(true);
				buscarMenuItem.setVisible(true);
			}


			// Titulo de la ActionBar 
			title = (String) getTitle();

			// Teclado
			imm = (InputMethodManager)getSystemService(
					getApplication().INPUT_METHOD_SERVICE);

			/*
			 * Ocultar teclado
			 */
			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			isSearchView = false;
		}

		return true;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:
			
			DetalleLocalidadFragmento.isDetalleLocalidadFragmento = false;
			
			Intent upIntent = NavUtils.getParentActivityIntent(this);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				// This activity is NOT part of this app's task, so create a new task
				// when navigating up, with a synthesized back stack.
				TaskStackBuilder.create(this)
				// Add all of this activity's parents to the back stack
				.addNextIntent(upIntent)
				// Navigate up to the closest parent
				.startActivities();
			}
			else {
				NavUtils.navigateUpTo(this, upIntent); 
			}

//			Utils.upHome(this);
			
			return true;

		case R.id.action_search:

			if (Utils.isNet() || MainActivity.LECTURA_DESDE_RAW) {
//				DetalleLocalidadFragmento.isNuevoItem = true;
				Intent intentNuevaL = new Intent(this, NuevaLocalidadActivity.class);
				startActivityForResult(intentNuevaL, REQUEST_CODE);
			}
			else {
				Toast.makeText(getApplicationContext(), getString(R.string.no_hay_conexion_internet), Toast.LENGTH_SHORT).show();
			}
				

			return true;

		case R.id.action_eliminar_detalle:

			// Para que no reinicie la actividad Detalle en refrescarDatalle()
			DetalleLocalidadFragmento.isEliminarItem = true;

			if (ListaLocalidadesFragmento.mDatosMeteoList.getSize() > 0) {
				int itemSelect = ListaLocalidadesFragmento.mItemActual;
				DatosMeteo datosMeteoSelect = ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList().get(itemSelect);

				AlertDialog.Builder builder = new AlertDialog.Builder(this)
				.setTitle(R.string.borrar_localidad_titulo)
				.setMessage(datosMeteoSelect.getNomLocalidad()
						+ "\n" + datosMeteoSelect.getNomPaisCiudad())
						.setPositiveButton(R.string.accept,
								new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {

								eliminarItem();

								if (!MainActivity.isTablet )   { //getResources().getBoolean(R.bool.tablet)) {
									/*
									 * Simula key back
									 */
									dispatchKeyEvent(new KeyEvent (KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
									dispatchKeyEvent(new KeyEvent (KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
								}
							}
						})

						.setNegativeButton("Cancelar",
								new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, 
									int which) {
								return;
							}
						});
				builder.show();
			}

			else
				Toast.makeText(getApplicationContext(), getString(R.string.bd_void), Toast.LENGTH_SHORT).show();

			return true;

		case R.id.action_eliminar:
			if (ListaLocalidadesFragmento.mDatosMeteoList.getSize() > 0) 
				eliminarItemDialogo();
			else
				Toast.makeText(getApplicationContext(), getString(R.string.bd_void), Toast.LENGTH_SHORT).show();


			return true;

		case R.id.action_reload:
			Toast.makeText(getApplicationContext(), "Se ha pulsado: action_reload", Toast.LENGTH_SHORT).show();
			
			// Para debug
//			MainActivity.isTablet = !MainActivity.isTablet;
//			MainActivity.mMainActivityThis.showListAndDetalleLocalidades();
			
//			Utils.reiniciarActivity(MainActivity.mMainActivityThis);
			
//			Utils.upHome(this);

//			getSupportFragmentManager().popBackStack();
			
//			onKeyDown(0, null);
			
			    
			return true;

		case R.id.action_mi_localización:
			MainActivity.mMainActivityThis.showLocation();

			return true;

		case R.id.action_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);

			return true;

			// Intent implicito, el sistema abre las aplicaiones a elegir
		case R.id.action_share:
			Intent intent1 = new Intent();
			intent1.setAction(Intent.ACTION_SEND);
			intent1.setType("text/plain");
			intent1.putExtra(Intent.EXTRA_TEXT, "mMeteoDatos");
			startActivity(intent1);

			return true;

		default:

			return super.onOptionsItemSelected(item);
		}
	}


	@Override
	public void afterTextChanged(Editable s) {

		if (isSearchView) {
			String findpalindrome=et.getText().toString();
			//setTitle(findpalindrome);
			String textIntro="";
			int length=findpalindrome.length();

			for (int i = 0; i<length; i++) {
				if (findpalindrome.charAt(i) == '\n') {

					//FindLocalidadesList(getTitle().toString());
					String stringSearchLocalidad = getTitle().toString(); 
					new BuscarLocalidadUrlTask().execute(stringSearchLocalidad);

					/*
					 * Oculta teclado
					 */
					imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
					//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

					/*
					 * Restaura tile
					 */
					setTitle(title);


					isSearchView = false;
				}

				else {
					textIntro = textIntro + findpalindrome.charAt(i);
					setTitle(textIntro);
				}


			}

		}

	}


	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}


	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	private void showFindLocalidadesList (DatosMeteoList datosMeteoList) {
		Intent intent = new Intent(this, NuevaLocalidadActivity.class); 
		intent.putExtra(NuevaLocalidadActivity.EXTRA,datosMeteoList);

		startActivityForResult(intent, CONFIRM_REQUEST_ID);
	} 



	/**===========================================================
	 * Recibe el resultado de la actividad NuevaLocalidadActivity
	 *============================================================*/
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case REQUEST_CODE: {
			if (resultCode == NuevaLocalidadActivity.CANCELLED) {
				Toast.makeText(this, "El usuario canceló", Toast.LENGTH_SHORT).show();
			}
			else {
				if(resultCode == RESULT_OK){      

					/*
					 * Recibiendo parametros. (aunque no los usare: sólo a efectos de ejercicio)
					 */
					// int result=Integer.parseInt(data.getStringExtra(ListViewAndroidExample.RETURN_RESULT));          				
					DatosMeteo datosMeteo = new DatosMeteo();
					datosMeteo = data.getParcelableExtra(NuevaLocalidadActivity.RETURN_RESULT);

					String name = datosMeteo.getNomLocalidad();
					double lat = datosMeteo.getLat();
					double log = datosMeteo.getLog();

					Log.d(BaseActionBarActivity.class.getSimpleName(), "onActivityResult recibe respuesta de la actividad NuevaLocalidadActivity.\n\n" 
							+ "Añadido: #\n" 
							+ name + " "
							+lat + ", ");

//					Toast.makeText(this, "onActivityResult recibe respuesta de la actividad NuevaLocalidadActivity.\n\n" 
//							+ "Añadido: #\n" 
//							+ name + " "
//							+lat + ", "
//							+log, Toast.LENGTH_LONG).show();


					ListaLocalidadesFragmento.mItemActual = ListaLocalidadesFragmento.mDatosMeteoList.getSize();
//					ListaLocalidadesFragmento.mItemActual++;
					MainActivity.mMainActivityThis.setDatosMeteoListFromSQL();
				}
			}
			break;
		}
		default: {
			super.onActivityResult(requestCode, resultCode, data);
			break;
		}
		}
	}// final onActivityResult()




	public class BuscarLocalidadUrlTask extends AsyncTask<String, Void, DatosMeteoList> {

		@Override
		protected DatosMeteoList doInBackground(String... params) {

			String stringSearchLocalidad = params[0];

			InputStream streamJSON = getResources()
					.openRawResource(R.raw.googleapis_lista_localidades);
			DatosMeteoList datosMeteoList = new DatosMeteoList();
			datosMeteoList = DatosMeteoList.parseJSONBusqueda(streamJSON);

			return datosMeteoList; 

		}

		@Override
		protected void onPostExecute(DatosMeteoList datosMeteoList) {
			//mDatosMeteoList = datosMeteoList;
			showFindLocalidadesList(datosMeteoList);
		}


	}// loadUrlTask





	public void eliminarItem() {

		/*
		 * Borra registro de base de datos 
		 */
//		while(WebServicio.isActualizandoseSQLfromWebTask){
//			Log.d(MenuActionBarActivity.class.getSimpleName(), "isActualizandoseSQLfromWebTask"); 
//		}
		
		int item = ListaLocalidadesFragmento.mItemActual;
		DatosMeteo datosMeteo = ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList().get(item);
		NotasDataSource dataSource;
		Nota nota = new Nota(); 
		nota.setTexto(datosMeteo.getNomLocalidad());
		nota.setId(datosMeteo.getId());

		dataSource = new NotasDataSource(MainActivity.mMainActivityThis);
		
		dataSource.open();
		dataSource.borrarNota(nota);

		dataSource.close();

		// Posición actual
		if(item == 0)
			ListaLocalidadesFragmento.mListaLocalidadesFragThis.mItemActual = 0;
		else 
			ListaLocalidadesFragmento.mListaLocalidadesFragThis.mItemActual = item -1;
		

		MainActivity.mMainActivityThis.setDatosMeteoListFromSQL();

	}



	public void eliminarItemDialogo() {

		int itemSelect2 = ListaLocalidadesFragmento.mItemActual;
		DatosMeteo datosMeteoSelect2 = ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList().get(itemSelect2);

		AlertDialog.Builder builder2 = new AlertDialog.Builder(this)
		.setTitle(R.string.borrar_localidad_titulo)
		.setMessage(datosMeteoSelect2.getNomLocalidad()
				+ "\n" + datosMeteoSelect2.getNomPaisCiudad())
				.setPositiveButton(R.string.accept,
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						eliminarItem();
					}
				})

				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog,
							int which) {
						// TODO Auto-generated method stub
						return;
					}
				});
		builder2.show();

	}

	
	
}
