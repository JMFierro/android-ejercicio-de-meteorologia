package com.jmfierro.utad.meteo.ActivPlus;



import com.jmfierro.utad.meteo.R;

import com.jmfierro.utad.meteo.Datos.DatosMeteoList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class DetalleLocalidadViewPagerAdapter extends FragmentStatePagerAdapter {
	
	private DatosMeteoList mDatosMeteoListViewPage;
	private DetalleLocalidadFragmento mFragment;
	
	
	public DetalleLocalidadViewPagerAdapter(FragmentManager fm, DatosMeteoList datosMeteoList) {
		super(fm);

		Log.d(DetalleLocalidadViewPagerAdapter.class.getSimpleName(),"DetalleLocalidadViewPagerAdapter()");

		this.mDatosMeteoListViewPage = datosMeteoList; 
		
	}

	@Override
	public Fragment getItem(int aItem) {
		
		Log.d(DetalleLocalidadViewPagerAdapter.class.getSimpleName(),"getItem()");

		mFragment = new DetalleLocalidadFragmento();
		Bundle fragmentData = new Bundle();
		
		/*
		 * Guarda la posicion del viewPager
		 */
//		if (aItem == 0)
//			ListaLocalidadesFragmento.mItemActual = 0;
//		else
//			ListaLocalidadesFragmento.mItemActual = aItem-1;
//		
		
		fragmentData.putParcelable(DetalleLocalidadFragmento.EXTRA, mDatosMeteoListViewPage.getdatosMeteoList().get(aItem));
		mFragment.setArguments(fragmentData);
		
//		// Argumentos a enviar al fragmento
//		Bundle arg = new Bundle();
//		arg.putParcelable(DetalleLocalidadFragmento.EXTRA, datosMeteo);
//		
//		// Creación de Fragmento					
//		DetalleLocalidadFragmento fragmento = new DetalleLocalidadFragmento();
//		fragmento.setArguments(arg);
//		getSupportFragmentManager().beginTransaction()
//		.replace(R.id.contenedorFrag_LocalidadDetalle,fragmento)
//		.commit();

		
		return mFragment;
	}

	@Override
	public int getCount() {
		Log.d(DetalleLocalidadViewPagerAdapter.class.getSimpleName(),"getCount()");
		
		return mDatosMeteoListViewPage.getdatosMeteoList().size(); 
	}

}
