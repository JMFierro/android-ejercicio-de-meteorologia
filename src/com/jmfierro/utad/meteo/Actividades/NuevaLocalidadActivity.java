package com.jmfierro.utad.meteo.Actividades;



import java.io.InputStream;

import org.json.JSONException;

import com.jmfierro.utad.meteo.R;
import com.jmfierro.utad.meteo.ActivPlus.ListaLocalidadesFragmento;
import com.jmfierro.utad.meteo.ActivPlus.BaseActionBarActivity;
import com.jmfierro.utad.meteo.ActivPlus.BaseActionBarActivity.BuscarLocalidadUrlTask;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Datos.DatosMeteoList;
import com.jmfierro.utad.meteo.Datos.Nota;
import com.jmfierro.utad.meteo.SQL.NotasDataSource;
import com.jmfierro.utad.meteo.Utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class NuevaLocalidadActivity extends Activity implements TextWatcher {

	public static final String EXTRA= "DatosMeteo";
	public static final String RETURN_RESULT = "return_result";
	public static final int ACCEPTED = 1;
	public static final int CANCELLED = 0;

	private Button btnAgregar;
	private EditText txtNota;
	private ListView mListView;

	// TextWatcher
	private String title;
	private boolean isSearchView;
	private InputMethodManager imm;
 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nueva_localidad);


		/*
		 * TextWatcher
		 */
		// Titulo de la ActionBar 
		title = (String) getTitle();  

		// Teclado
		imm = (InputMethodManager)getSystemService(
				getApplication().INPUT_METHOD_SERVICE);

		// EditText para la busqueda
		txtNota=(EditText)findViewById(R.id.textLocalidad);
		txtNota.addTextChangedListener(this);
		txtNota.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				/*
				 * Reinicia la actividad
				 */
				Intent intent = getIntent();
				finish();
				startActivity(intent);
			}

		});


		// Lectura del texto introducido por el suario.
		isSearchView = true;


		btnAgregar = (Button) findViewById(R.id.btnCancel);
		btnAgregar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) { 
				String textoNota = txtNota.getText().toString();

				setResult(RESULT_CANCELED);
				finish();
			}
		});

	}



	@Override
	public void afterTextChanged(Editable s) {

		if (isSearchView) {
			String findpalindrome=txtNota.getText().toString();

			// Va guardando el texto
			String textIntro="";

			int length=findpalindrome.length();

			for (int i = 0; i<length; i++) {
				if (findpalindrome.charAt(i) == '\n') {


					/*
					 * Oculta teclado
					 */
					imm.hideSoftInputFromWindow(txtNota.getWindowToken(), 0);


					isSearchView = false;
					txtNota.setText(textIntro);

					/* ------------------------------------------------------- 
					 * Finalizará despues de mostrar las opciones encontradas
					 * y elegir una 
					 *--------------------------------------------------------*/


					if (Utils.isNet() || MainActivity.LECTURA_DESDE_RAW) {
						LoadUrlListaTask loadUrlTask = new LoadUrlListaTask();
						loadUrlTask.execute(textIntro, Utils.CONSULTA_FIND);
					}
				}

				else {
					textIntro = textIntro + findpalindrome.charAt(i);
				}

			}
		}

	}


	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}


	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}


	/*
	 * Listado de localidades encontradas
	 */
	public void showListFindLocalidades(final DatosMeteoList datosMeteoList) {

		mListView = (ListView) findViewById(R.id.list);

		String [] arraystringLocalidades  = new String[datosMeteoList.getSize()];
		for (int i=0; i<datosMeteoList.getSize(); i++) {
			String nombPaisCiudad = datosMeteoList.getdatosMeteoList().get(i).getNomPaisCiudad();
			arraystringLocalidades[i] = datosMeteoList.getdatosMeteoList().get(i).getNomLocalidad()
										+ "\n" + nombPaisCiudad;
//			arraystringLocalidades[i] = datosMeteoList.getdatosMeteoList().get(i).getNomPaisCiudad()
//					+ "\n(" + datosMeteoList.getdatosMeteoList().get(i).getNomLocalidad() + ")";
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1,arraystringLocalidades);

		mListView.setAdapter( adapter); 
		mListView.setOnItemClickListener(new OnItemClickListener() {


			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// ListView Clicked item index
				int itemPosition     = position;

				// ListView Clicked item value
				String  itemValue    = (String) mListView.getItemAtPosition(position);

				DatosMeteo datosMeteoResul = datosMeteoList.getdatosMeteoList().get(position);
				Intent returnIntent = new Intent();

				/*
				 * Añade elemento a la base de datos y actualiza registro el objeto DatosMeta. 
				 */
				String stringDatosMeteoNota = datosMeteoResul.getTextNota();
				NotasDataSource dataSource;

				dataSource = new NotasDataSource(MainActivity.mMainActivityThis); //this);
				dataSource.open();

				dataSource.crearNota(stringDatosMeteoNota);
				Nota nota = dataSource.getNotas(stringDatosMeteoNota);
				datosMeteoResul.setId(nota.getId());

				dataSource.close();


				/*
				 * ...de vuelta a la clase BasectionBarActivity.java en el método onActivityResult().
				 */
				returnIntent.putExtra(RETURN_RESULT,datosMeteoResul);
				setResult(RESULT_OK,returnIntent);     
				finish();


			}

		}); 

	}

	public class LoadUrlListaTask extends AsyncTask<Object, Void, DatosMeteoList> {

		/*
		 * Actualiza un DatosMeteoList.
		 */
		@Override
		protected DatosMeteoList doInBackground(Object... params) {
			long lat,log;

			String stringUrl = (String) params[0];
			String  tiposConsulta =  (String) params[1];

			String stringJSONfind=null;
			InputStream streamJSONfind = null;

			if (Utils.isNet()) {
				stringJSONfind = Utils.downloadUrl(stringUrl, tiposConsulta);
			}
			else if (MainActivity.LECTURA_DESDE_RAW) {

				streamJSONfind = getApplication().getResources()
						.openRawResource(R.raw.openweathermap_find_leon);
				stringJSONfind = Utils.stringJSONfromStream(streamJSONfind);
			}

			// ** no funciona **
			//			DatosMeteoList datosMeteoListFind2 = new DatosMeteoList(streamJSON);
			// ---------------------------------
			DatosMeteoList datosMeteoListFind = new DatosMeteoList(stringJSONfind);
			DatosMeteoList datosMeteoListLleno = new DatosMeteoList(); 
			DatosMeteo datosMeteo;

			InputStream streamJSONitem;
			String stringJSONitem = null;
			for (int i=0; i< datosMeteoListFind.getSize(); i++){
				lat = datosMeteoListFind.getdatosMeteoList().get(i).getLat();
				log = datosMeteoListFind.getdatosMeteoList().get(i).getLog();
				if (Utils.isNet()) {
					stringJSONitem = Utils.downloadUrl(lat,log);
				}
				else if (MainActivity.LECTURA_DESDE_RAW) {
					if (i==0)
						streamJSONitem = getApplication().getResources()
						.openRawResource(R.raw.openweathermap_weather_madrid);
					else 
						streamJSONitem = getApplication().getResources()
						.openRawResource(R.raw.openweathermap_weather_inventado);
					stringJSONitem = Utils.stringJSONfromStream(streamJSONitem);
				}

				String nombPaisCiudad= datosMeteoListFind.getdatosMeteoList().get(i).getNomLocalidad();
				nombPaisCiudad = nombPaisCiudad.replace(","," -");
				datosMeteo = new DatosMeteo(stringJSONitem,"("+nombPaisCiudad+")");
				String nombre = datosMeteo.getNomLocalidad();
				String nombrebis = datosMeteo.getNomPaisCiudad();
				nombre = nombre.replace(",","-");
				nombrebis = nombrebis.replace(","," -");
				datosMeteo.setNomLocalidad(nombre);
				datosMeteo.setNomCiudadPais(nombrebis);

				datosMeteoListLleno.add(datosMeteo);
			}
			return datosMeteoListLleno;
		}



		@Override
		protected void onPostExecute(DatosMeteoList result) {

			showListFindLocalidades(result);
		}

	}
	public class LoadUrlSelectTask extends AsyncTask<Object, Void, DatosMeteo> {

		/*
		 * Actualiza una localización.
		 */
		@Override
		protected DatosMeteo doInBackground(Object... params) {

			long lat = (Long) params[0];
			long log = (Long) params[1];

			String stringJSONitem =null;
			InputStream streamJSONitem = null;
			if (Utils.isNet()) {
				stringJSONitem = Utils.downloadUrl(lat, log);
			}
			else if (MainActivity.LECTURA_DESDE_RAW) {

				streamJSONitem = getApplication().getResources()
						.openRawResource(R.raw.openweathermap_weather_madrid);
				stringJSONitem = Utils.stringJSONfromStream(streamJSONitem);
			}

			DatosMeteo datosMeteo = null;
			datosMeteo = new DatosMeteo(stringJSONitem,"");
			return datosMeteo;
		}

		/*
		 * Añade nuevo registro a la base de datos
		 */
		@Override
		protected void onPostExecute(DatosMeteo result) {


			DatosMeteo datosMeteoResul = result;
			Intent returnIntent = new Intent();


			/*
			 * Añade elemento a la base de datos y actualiza registro el objeto DatosMeta. 
			 */
			String stringDatosMeteoNota = datosMeteoResul.getTextNota();
			NotasDataSource dataSource;

			dataSource = new NotasDataSource(MainActivity.mMainActivityThis); //this);
			dataSource.open();

			dataSource.crearNota(stringDatosMeteoNota);
			Nota nota = dataSource.getNotas(stringDatosMeteoNota);
			datosMeteoResul.setId(nota.getId());

			dataSource.open();


			/*
			 * ...de vuelta a la clase MeteoMenuActionBarActivity.java en el método onActivityResult().
			 */
			returnIntent.putExtra(RETURN_RESULT,datosMeteoResul);
			setResult(RESULT_OK,returnIntent);     
			finish();
		}

	}

}
