package com.jmfierro.utad.meteo.Actividades;

import com.jmfierro.utad.meteo.R;
import com.jmfierro.utad.meteo.ActivPlus.DetalleLocalidadFragmento;
import com.jmfierro.utad.meteo.ActivPlus.DetalleLocalidadViewPagerAdapter;
import com.jmfierro.utad.meteo.ActivPlus.ListaLocalidadesFragmento;
import com.jmfierro.utad.meteo.ActivPlus.BaseActionBarActivity;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Datos.DatosMeteoList;
import com.jmfierro.utad.meteo.R.id;
import com.jmfierro.utad.meteo.R.layout;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class DetalleLocalidadActivity extends BaseActionBarActivity {

	public static final String EXTRA = "DatosMeteo"; 
	public static final String EXTRA_ITEM = "item_actual"; 
	private DatosMeteoList mDatosMeteoListDetalleLocalidad; 

	private ViewPager mPager;
	private DetalleLocalidadViewPagerAdapter mPagerAdapter;
	private int mItemActual;

	
	public static Activity mActivity;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detalle_localidad);
		
		// Obtiene los datos meteorologicos.
		Intent intent= this.getIntent();
		mDatosMeteoListDetalleLocalidad = intent.getParcelableExtra(EXTRA);
		mItemActual = intent.getIntExtra(EXTRA_ITEM,0);
		if (mDatosMeteoListDetalleLocalidad == null){ 
			finish();
			return;
		}


		// Implementacion PageView
		mPager= (ViewPager) findViewById(R.id.detalle_localidad_view_pager);
		//mPostList = PostList.getInstance();
		mPagerAdapter = new DetalleLocalidadViewPagerAdapter (getSupportFragmentManager(), mDatosMeteoListDetalleLocalidad);
		mPager.setAdapter(mPagerAdapter);

		mPager.setCurrentItem(ListaLocalidadesFragmento.mItemActual); 
		
		mPager.setOnPageChangeListener(new OnPageChangeListener() {
		    public void onPageScrollStateChanged(int state) {}
		    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

		    public void onPageSelected(int position) {
		    	ListaLocalidadesFragmento.mItemActual = position;
//		    	mItemActual = mPager.getCurrentItem();
		    }
		});
	
	}


	
	
}
