package com.jmfierro.utad.meteo.Actividades;
// package="com.utad.android.meteojmfierro"


import java.util.List;


import com.jmfierro.utad.meteo.ActivPlus.DetalleLocalidadFragmento;
import com.jmfierro.utad.meteo.ActivPlus.ListaLocalidadesFragmento;
import com.jmfierro.utad.meteo.ActivPlus.BaseActionBarActivity;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Datos.DatosMeteoList;
import com.jmfierro.utad.meteo.Datos.Nota;
import com.jmfierro.utad.meteo.SQL.NotasDataSource;
import com.jmfierro.utad.meteo.Utils.Utils;

import com.jmfierro.utad.meteo.Web.WebServicio;
import com.jmfierro.utad.meteo.Web.WebServicio.WebBinder;
import com.jmfierro.utad.meteo.R;

import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends BaseActionBarActivity implements ListaLocalidadesFragmento.Callbacks,OnClickListener {


	// Para debug
	public static boolean PRUEBA = false; 
	public static boolean LECTURA_DESDE_RAW = PRUEBA;
	
	public static boolean isTablet;
	
	public static MainActivity mMainActivityThis; 

	// Servicio
	private WebBinder mWebBinder; 
	static private boolean isSQLActualizada = false; 

	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;

	//Variables para la geolocalización
	private LocationManager mManager = null;
	private LocationListener mListener = null;
	private Geocoder mGeocoder = null;

	private double mLongitud;
	private double mLatitud;
	private String mCiudad ;

	// BlogLoadedReceiver
	private MeteoLoadedReceiver mMeteoLoadedReceiver = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(MainActivity.class.getSimpleName(),"----------");
		Log.d(MainActivity.class.getSimpleName(),"onCreate()");
		Log.d(MainActivity.class.getSimpleName(),"-----------");

//		boolean mProviderEnable = false; 

		mMainActivityThis = this;

		setLocalizacion();
		
		isTablet = getResources().getBoolean(R.bool.tablet);
		setContentView(R.layout.meteo_main);
//		
//		if (isTablet)
//			setContentView(R.layout.main_dospaneles);
//		else
//			setContentView(R.layout.main_unpanel);


		if (!isSQLActualizada) {

			/*---------------------------------------------------------
			 * Muestra los datos de la base de datos sin actualizar, 
			 * para que el usuario no tenga la pantalla en blanco.
			 * 
			 * (...más adelante cuando el servicio termine  
			 * con la base de datos actualizada se repintará la vista.)
			 *-------------------------------------------------------*/
			setDatosMeteoListFromSQL();

			/*
			 * Actualiza base de datos
			 */
			if (Utils.isNet() || MainActivity.LECTURA_DESDE_RAW)
				conexionWebServicio();
			
			isSQLActualizada=true;
		}
		else
			setDatosMeteoListFromSQL();


		// Registro del MeteoLoadedReceiver
		Log.d(MainActivity.class.getSimpleName(), "...[desde onCreate()] Registro del MeteoLoadedReceiver" );
		Log.d(MainActivity.class.getSimpleName(), "Registro del MeteoLoadedReceiver");
		IntentFilter filter = new IntentFilter( WebServicio.LOADED_ACTION );
		mMeteoLoadedReceiver = new MeteoLoadedReceiver();
		this.registerReceiver(mMeteoLoadedReceiver, filter);

		/*
		 * Menu lateral
		 */
		// DrawerToggle: con el icono home de la ActionBar se abre o se cierra el Drawer
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
			}
		};
		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// Listeners botones Drawer
		findViewById(R.id.left_drawer).setOnClickListener(this);
		
		ImageView isobarasImageView = (ImageView) findViewById(R.id.menuLateral_imgIsobaras);
		isobarasImageView.setImageDrawable(getResources().getDrawable(R.drawable.isobaras));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);


	}


	
	
	private void setLocalizacion() {

		ListaLocalidadesFragmento.mItemActual=0;


		// Get a LocationManager, its listener and a Geocoder
		mManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		mListener = new MyLocationListener();
		mGeocoder = new Geocoder(this);

		// Location updates provided by the NETWORK_PROVIDER should be handled by the provide listener
		mManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 15000, 0, mListener);
		Toast.makeText(this, LocationManager.NETWORK_PROVIDER, Toast.LENGTH_SHORT).show();
		Log.d(MainActivity.class.getSimpleName(),"LocationManager.NETWORK_PROVIDER" + LocationManager.NETWORK_PROVIDER);

//		mProviderEnable=true;
		
		//Obtiene localización al iniciar
		Location currentLocation = mManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER); 
		if (currentLocation != null) {
			mLatitud = currentLocation.getLatitude();
			mLongitud = currentLocation.getLongitude();
		}
		
		
	}




	private void conexionWebServicio(){
		Log.d(MainActivity.class.getSimpleName(), "conexionWebServicio()" );
		Log.d(MainActivity.class.getSimpleName(),"startService (service");

		Intent service = new Intent(MainActivity.this, WebServicio.class);
		startService(service);
		//		bindService(service, mWebServiceConn, Service.BIND_AUTO_CREATE); 
	}



	/**========================================================================================
	 * CallBack que recibe el item seleccionado desde MeteoListaLocalidadesFragmento.Callbacks
	 *=========================================================================================*/
	@Override
	public void onMyItemViewSeleccionado(int posicion) {

		DatosMeteo datosMeteoSeleccionada = new DatosMeteo();
		//		datosMeteoSelecciona = mDatosMeteoListMain.getdatosMeteoList().get(posicion);
		datosMeteoSeleccionada = ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList().get(posicion);

		Toast.makeText(this, getString(R.string.toast_pulsado) + datosMeteoSeleccionada.getId() 
				+" "+ datosMeteoSeleccionada.getNomLocalidad(), Toast.LENGTH_SHORT).show();

		/*---------------------------------
		 * Pantallas a partir de 7 pulgadas
		 *---------------------------------*/
		if (isTablet )   { //getResources().getBoolean(R.bool.tablet)) {
//			refrescarDetalleLocalidad(datosMeteoSeleccionada);
			showListAndDetalleLocalidades();
		}

		else {
			Intent intent = new Intent(MainActivity.this,DetalleLocalidadActivity.class);
			intent.putExtra(DetalleLocalidadActivity.EXTRA, ListaLocalidadesFragmento.mDatosMeteoList);
			intent.putExtra(DetalleLocalidadActivity.EXTRA_ITEM, posicion);
			startActivity(intent);
		}
	}


	/**==============================================
	 * Callbacks de MeteoListaLocalidadesFragmento
	 * Recibe los datos de la lista
	 *===============================================*/
//	public void onSetDatosMeteoListFromSQL(DatosMeteoList datosMeteoList) {
//
//		Log.d(MainActivity.class.getSimpleName(), "onSetDatosList()" );
//
//		setDatosMeteoListFromSQL();
//
//	}

	public void setDatosMeteoListFromSQL() {

		Log.d(MainActivity.class.getSimpleName(), "setDatosMeteoList()" );

		new SetDatosMeteoListFromSQLAsyncTask().execute();

	}



	/**===================================
	 * Actualiza los datos del detalle
	 *====================================*/
	public void refrescarDetalleLocalidad () {
		
		if (ListaLocalidadesFragmento.mDatosMeteoList.getSize() == 0)
			refrescarDetalleLocalidad(null);
		else {

			int itemActual = ListaLocalidadesFragmento.mItemActual;  
			DatosMeteoList datosMeteoList = ListaLocalidadesFragmento.mDatosMeteoList; 
				if (itemActual > datosMeteoList.getSize())
					itemActual = ListaLocalidadesFragmento.mItemActual = datosMeteoList.getSize();

				DatosMeteo datosMeteo = new DatosMeteo();  
				datosMeteo = datosMeteoList.getdatosMeteoList().get(itemActual);
				refrescarDetalleLocalidad(datosMeteo);
		}
	}
	

	
	public void refrescarDetalleLocalidad (DatosMeteo datosMeteo) {

		Log.d(MainActivity.class.getSimpleName(),"refrescarDetalleLocalidad()");  

		// Argumentos a enviar al fragmento
		Bundle arg = null;
//		if (datosMeteo == null) 
//			datosMeteo = new DatosMeteo();
			
//		if (datosMeteo != null) { 
			arg = new Bundle();
			arg.putParcelable(DetalleLocalidadFragmento.EXTRA, datosMeteo);

			// Creación de Fragmento					
			DetalleLocalidadFragmento fragmento = new DetalleLocalidadFragmento();
			fragmento.setArguments(arg);
			getSupportFragmentManager().beginTransaction()
			.replace(R.id.contenedorFrag_LocalidadDetalle,fragmento)
			.commit();
//		}
		
//		else {
//			if (DetalleLocalidadFragmento.mActivity != null)
//				DetalleLocalidadFragmento.mActivity.finish();
//		}
//
//		else {
//			if (DetalleLocalidadFragmento.mActivity != null)
//				Utils.reiniciarActivity(DetalleLocalidadFragmento.mActivity);
//		}


		// Implementacion PageView tambien en tablet
		//		 ViewPager mPager;
		//		 DetalleLocalidadViewPagerAdapter mPagerAdapter;
		//
		//		mPager= (ViewPager) findViewById(R.id.detalle_localidad_view_pager);
		//		//mPostList = PostList.getInstance();
		//		mPagerAdapter = new DetalleLocalidadViewPagerAdapter (getSupportFragmentManager(), ListaLocalidadesFragmento.mDatosMeteoList);
		//		mPager.setAdapter(mPagerAdapter);
		//		
		//		mPager.setCurrentItem(ListaLocalidadesFragmento.mItemActual);

	}


	
	public void showListAndDetalleLocalidades() { 

		Log.d(MainActivity.class.getSimpleName(), "showDatosMeteoListAndDetalle()"); 

		ListaLocalidadesFragmento.mListaLocalidadesFragThis.refrescarListaLocalidades();
		
		this.supportInvalidateOptionsMenu();
		
		/*
		 * Refresca datos detalle.
		 */
		if (isTablet )   { 
//			int itemActual = ListaLocalidadesFragmento.mItemActual;  
//			DatosMeteoList datosMeteoList = ListaLocalidadesFragmento.mDatosMeteoList; 
//			if (datosMeteoList.getSize() > 0) { 
//				if (itemActual > datosMeteoList.getSize())
//					itemActual = ListaLocalidadesFragmento.mItemActual = datosMeteoList.getSize();
//
//				DatosMeteo datosMeteo = new DatosMeteo();  
//				datosMeteo = datosMeteoList.getdatosMeteoList().get(itemActual);
//				refrescarDetalleLocalidad(datosMeteo);
			refrescarDetalleLocalidad();
//			}
		}
		
		/*
		 * En caso de pantalla menores a 7 pulgadas,  
		 * reinicia la actividad detalle si es visible.
		 */
		else {
			if (DetalleLocalidadFragmento.isDetalleLocalidadFragmento) {
				if (DetalleLocalidadFragmento.mActivity != null) {

					DetalleLocalidadFragmento.mActivity.finish();

//					if (DetalleLocalidadFragmento.isNuevoItem) {
					if (!DetalleLocalidadFragmento.isEliminarItem) {
//						DetalleLocalidadFragmento.isNuevoItem = false;
						Intent intent = new Intent(MainActivity.this,DetalleLocalidadActivity.class);
						intent.putExtra(DetalleLocalidadActivity.EXTRA, ListaLocalidadesFragmento.mDatosMeteoList);
						int posicion = ListaLocalidadesFragmento.mItemActual;
						intent.putExtra(DetalleLocalidadActivity.EXTRA_ITEM, posicion);
						startActivity(intent);
					}
					else
						DetalleLocalidadFragmento.isEliminarItem = false;

				}
			}
		}
	}



	
	
	/*
	 * Nuevo hilo para leer base de datos SQL
	 * Ultima modificación:	4/1/2014
	 */
	class SetDatosMeteoListFromSQLAsyncTask extends AsyncTask<Void, Void, Object> {

		@Override
		protected void onPreExecute() {
			Log.d(MainActivity.class.getSimpleName(), "SetDatosMeteoListFromSQLAsyncTask()..." );
			Log.d(MainActivity.class.getSimpleName(), "...onPreExecute()" );
		}


		/*
		 * Ejecucion hilo lectura SQL
		 */
		@Override
		protected DatosMeteoList doInBackground(Void... params) {

			Log.d(MainActivity.class.getSimpleName(), "SetDatosMeteoListFromSQLAsyncTask()..." );
			Log.d(MainActivity.class.getSimpleName(), "...doInBackground()..." );

			// Obtiene datoMeteo de la base de datos
			Log.d(MainActivity.class.getSimpleName(),"Obtiene datos de la base de datos");

			List<Nota> listaNotas = NotasDataSource.getAllNotasOpenClose(); 
			//			List<Nota> listaNotas = dataSource.getAllNotas(); 
			DatosMeteoList datosMeteoListFromSQL = new DatosMeteoList(listaNotas);
//			Log.d(MainActivity.class.getSimpleName(), "...mDatosMeteoList ACTUALIZADO !!!");


			return datosMeteoListFromSQL;
		}

		@Override
		protected void onPostExecute(Object result) {

			Log.d(MainActivity.class.getSimpleName(), "SetDatosMeteoListFromSQLAsyncTask()..." );
			Log.d(MainActivity.class.getSimpleName(), "...onPostExecute()..." );

			// Lee los datos de la base de datos que se mostraran en el listview. (tb. detalle)
			ListaLocalidadesFragmento.mDatosMeteoList = (DatosMeteoList) result;
			Log.d(MainActivity.class.getSimpleName(), "...mDatosMeteoList ACTUALIZADO !!!");

			showListAndDetalleLocalidades();

		}


	} // FIN clase AnyncTazk

	public void showLocation() {
//		if (mLatitud == 0 && mLongitud == 0) {
		if (Utils.isNet()) 
			Toast.makeText(getApplicationContext(), getString(R.string.localizacion_actual) + "\n Lat: " + mLatitud + " " + "\n Long: " + mLongitud , Toast.LENGTH_LONG).show();
		else 
			Toast.makeText(getApplicationContext(), getString(R.string.no_hay_conexion_internet) , Toast.LENGTH_LONG).show();
		
		Log.d(MainActivity.class.getSimpleName(),"showLocation():" + "Lat:" + mLatitud + " " + "long:" + mLongitud );
	}

	// Listener for managing location changes
	final class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {

			Log.d(MainActivity.class.getSimpleName(), "onLocationChanged()");

			// TODO Auto-generated method stub
			// Update the information in the interface "longitude,latitude" 
			mLongitud = location.getLongitude();
			mLatitud = location.getLatitude();
//			showLocation();
//			Toast.makeText(getApplicationContext(), "class MyLocationListener: onLocationChanged()" , Toast.LENGTH_LONG).show();
			// 			WeatherTask wt = new WeatherTask();
			//			//A�adimos una referentcia del padre
			//			wt.setContext(mMeteoContexto);
			//			wt.execute(mLatitud,mLongitud);
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
		}

	}


	/**
	 * Recibe los mensajes con action "com.jmfierro.utad.meteo.Actividades.LOADED_ACTION"
	 */
	private class MeteoLoadedReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(MainActivity.class.getSimpleName(), "onReceive():");

			Log.d(MainActivity.class.getSimpleName(), "+++++++++++++++++++++++++++++++++++++++++++++++++");
			Log.d(MainActivity.class.getSimpleName(), "MeteoLoadedReceiver - onReceive()");
			Log.d(MainActivity.class.getSimpleName(), "BroadcastReceiver ha terminado de actualizar SQL");
			Log.d(MainActivity.class.getSimpleName(), "+++++++++++++++++++++++++++++++++++++++++++++++++");

			/*
			 * lee base de datos
			 */
			if (intent.getAction().equals( WebServicio.LOADED_ACTION )) {
				Log.d(MainActivity.class.getSimpleName(), "...intent.getAction().equals( WebServicio.LOADED_ACTION )...");
				Log.d(MainActivity.class.getSimpleName(), "...lee base de datos: actualiza mDatosMeteoList...");

				DatosMeteoList datosMeteoListReceiver = intent.getParcelableExtra(WebServicio.LOADED_ACTION_DATOSMETEOLIST);
				setDatosMeteoListFromSQL();
			}

		}

	}


	/*
	 * Actualiza base de datos
	 */
//	public void actualizaSQL(DatosMeteoList datosMeteoListActualizado) {
//
//		NotasDataSource dataSource = null;
//		dataSource = new NotasDataSource(MainActivity.mMainActivityThis); //this);
//		dataSource.open();
//		List<Nota> listaNotas = dataSource.getAllNotas();
//		
//		long lat,log;
//
//		for(int i = 0; i < datosMeteoListActualizado.getSize(); i++) { 
//			
////			long idOfListaNotas = listaNotas.get(i).getId();
//			DatosMeteo datosMeteoActualizado = datosMeteoListActualizado.getdatosMeteoList().get(i);
//
//			Nota nota = dataSource.getNotas(datosMeteoActualizado.getId());
//			nota.setTexto(datosMeteoActualizado.getTextNota());
//		
//		}
//
//
//		dataSource.close();

		
//		ListView lvNotas;
//		NotasDataSource dataSource = null;
//		dataSource = new NotasDataSource(MainActivity.mMainActivityThis); //this);
//		dataSource.open();
//		List<Nota> listaNotas = dataSource.getAllNotas();
//		
//		long lat,log;
//
//		for(int i = 0; i < listaNotas.size(); i++) { 
//
//			long idOfListaNotas = listaNotas.get(i).getId();
//			DatosMeteo datosMeteoOld = new DatosMeteo(listaNotas.get(i).gettexto(),idOfListaNotas);
//
//			
//			/*
//			 * Baja datos de red
//			 */
//			lat = datosMeteoListFromNota.getdatosMeteoList().get(i).getLat();
//			log = datosMeteoListFromNota.getdatosMeteoList().get(i).getLog();
//			if (Utils.isNet()) {
//				stringJSONitem = Utils.downloadUrl(lat,log);
//			}
//			else if (MainActivity.LECTURA_DESDE_RAW) {
//				streamJSONitem = getApplication().getResources()
//						.openRawResource(R.raw.openweathermap_weather_madrid);
//				stringJSONitem = Utils.stringJSONfromStream(streamJSONitem);
//			}
//
//			String nombPaisCiudad = datosMeteoOld.getNomPaisCiudad();
//			DatosMeteo datosMeteoNew = new DatosMeteo(stringJSONitem, nombPaisCiudad);
//			
//			
//			/*
//			 * Actualiza base de datos
//			 */
//			Nota nota = dataSource.getNotas(idOfListaNotas);
//			String textNotaNuevaString = datosMeteoNew.getTextNota();
//			datosMeteoOld.setFecha(Utils.getFechaActual());
//			nota.setTexto(datosMeteoOld.getTextNota());
//			int j = 0;
//			j = 9;
//			datosMeteoListNew.add(datosMeteoOld);
//		
//		}
//
//
//		dataSource.close();

//	}
	

	@Override
	protected void onStart() {
		super.onStart();

		Log.d(MainActivity.class.getSimpleName(), "onStart()");

	}




	@Override
	protected void onStop() {

		// 		desconexionWebService();
		Log.d(MainActivity.class.getSimpleName(), "onStop()");

		super.onStop();
	}



	@Override
	protected void onDestroy() {
		Log.d(MainActivity.class.getSimpleName(), "onDestroy()");
		Log.d(MainActivity.class.getSimpleName(), "...unregisterReceiver (mMeteoLoadedReceiver)");
		this.unregisterReceiver(mMeteoLoadedReceiver);

		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		Toast.makeText(this, getString(R.string.toast_pulsado_menu_lateral), Toast.LENGTH_LONG).show();

	}


	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		Log.d(MainActivity.class.getSimpleName(), "onPostCreate()");

		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		Log.d(MainActivity.class.getSimpleName(), "onConfigurationChanged()");

		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		Log.d(MainActivity.class.getSimpleName(), "onOptionsItemSelected()");

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle your other action bar items...

		return super.onOptionsItemSelected(item);
	}


	
//	public void setIsMovilSimulation() {
//		
//		// Lee las preferencias del usuario.
//		String repeatingPreference = PreferenceManager.getDefaultSharedPreferences(this)
//				.getString(
//						"sync_frequency", 
//						"" + getResources().getInteger(R.integer.pref_sync_frequency_default));
//		
//		int repeatingMillis = Integer.parseInt(repeatingPreference) * 60 * 1000; // minutes to milliseconds
//
//	}
	
}
