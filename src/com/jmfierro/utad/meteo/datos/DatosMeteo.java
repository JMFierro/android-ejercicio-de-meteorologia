package com.jmfierro.utad.meteo.Datos;



import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.jmfierro.utad.meteo.Utils.Utils;

import android.net.ParseException;
import android.os.Parcel;
import android.os.Parcelable;

public class DatosMeteo implements Parcelable{

	private String nombLocalidad;
	private String nombCiudadPais;
	private long id;
	private String main;
	private String descripcion;
	private String icon;
	private String fecha;

	private long log;
	private long lat;

	// Varios
	private String temp;
	private String temp_min; 
	private String temp_max;

	private String presion; 
	private String humedad; 

	//Viento
	private String velocidad; 
	private String grado;

	//General
	//private Bitmap img;


	//private Date fecha=new Date("00-00-0000");

	private String textNota;
	private String nota;

	/**==================
	 * Constructor vacio
	 *===================*/
	public DatosMeteo() {

		new DatosMeteo(0,0,
				null,null,null,null,null,
				null,null,
				0,null,null,null,
				null,null,null,null, null);  // Date("00-00-0000"));
	}


	/**==================
	 * Constructor set
	 *===================*/
	public DatosMeteo(long log, 
			long lat,
			String temp,
			String presion,
			String humedad,
			String temp_min,
			String temp_max,
			String velocidad,
			String grado,
			long id,
			String main,
			String descripcion,
			//Bitmap img,
			String nombLocalidad,
			String nombCiudadPais,
			String fecha,
			String icon,
			String textNota,
			String nota)
	{

		this.log = log; 
		this.lat = lat;
		this.temp = temp;
		this.presion = presion;
		this.humedad = humedad;
		this.temp_min = temp_min;
		this.temp_max = temp_max;
		this.velocidad = velocidad;
		this.grado = grado;
		this.id = id;
		this.main = main;
		this.descripcion = descripcion;
		//this.img  =img;
		this.nombLocalidad=nombLocalidad;
		this.nombCiudadPais=nombCiudadPais;
		this.fecha=fecha;
		this.icon=icon;

		this.textNota=textNota;
		this.nota=nota;

	}


	/**==================
	 * Constructor set
	 *===================*/
	public DatosMeteo(long log, 
			long lat,
			String temp,
			String presion,
			String humedad,
			String temp_min,
			String temp_max,
			String velocidad,
			String grado,
			long id,
			String main,
			String descripcion,
			String nombLocalidad,
			String nombCiudadPais,
			String icon,
			String fecha,
			String textNota)
	{

		this.log = log; 
		this.lat = lat;
		this.temp = temp;
		this.presion = presion;
		this.humedad = humedad;
		this.temp_min = temp_min;
		this.temp_max = temp_max;
		this.velocidad = velocidad;
		this.grado = grado;
		this.id = id;
		this.main = main;
		this.descripcion = descripcion;
		this.nombLocalidad=nombLocalidad;
		this.nombCiudadPais=nombCiudadPais;
		this.icon=icon;
		this.fecha=fecha;
		this.textNota=textNota;

	}





	/**=====================================================
	 * Constructores para JSON (String, Stream y JSONObject)
	 *======================================================
	 * @throws JSONException */
	public DatosMeteo(String stringJSON, String nombPaisCiudad) {

		//		this(new JSONObject(stringJSON));
		JSONObject objectJSON = null;
		try {
			objectJSON = new JSONObject(stringJSON);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if ( objectJSON != null) {

			try {
				this.nombLocalidad = objectJSON.getString("name");

				JSONObject objectcoord = objectJSON.getJSONObject("coord");
				this.lat = objectcoord.getLong("lat");
				this.log = objectcoord.getLong("lon");

				JSONArray array = objectJSON.getJSONArray("weather");
				JSONObject object = array.getJSONObject(0);

				this.main = object.getString("main");
				this.descripcion  = object.getString("description");
				this.icon  = object.getString("icon");

				this.fecha = Utils.getFechaActual();


				JSONObject object2 = objectJSON.getJSONObject("main");
				this.temp = object2.getString("temp");
				this.temp_min = object2.getString("temp_min");
				this.temp_max = object2.getString("temp_max");

				this.presion = object2.getString("pressure");
				this.humedad = object2.getString("humidity");

				JSONObject object3 = objectJSON.getJSONObject("wind");
				this.velocidad = object3.getString("speed");
				this.grado = object3.getString("deg");

				setNomCiudadPais(nombPaisCiudad);
				setTextNota();

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public DatosMeteo(String textNota, long id) {

		this();

		final CharSequence cs = textNota;

		ArrayList<String> arrayDatosMeteo = new ArrayList<String>();
		String[] parString = cs.toString().split(",");

		for(int k=0;k<parString.length;k++) {
			final CharSequence cs2 = parString[k].toString();
			String[] vals2 = cs2.toString().split("=");
			arrayDatosMeteo.add(vals2[1].toString());    
		}

		this.setNota(textNota);
		this.setId(id);

		this.setNomLocalidad(arrayDatosMeteo.get(1));
		this.setNomCiudadPais(arrayDatosMeteo.get(2));
		this.setMain(arrayDatosMeteo.get(3));
		this.setIcon(arrayDatosMeteo.get(4));
		this.setDescripcion(arrayDatosMeteo.get(5));
		this.setFecha(arrayDatosMeteo.get(6));

		this.setLat(Long.parseLong(arrayDatosMeteo.get(7)));
		this.setLog(Long.parseLong(arrayDatosMeteo.get(8)));

		this.setPresion(arrayDatosMeteo.get(9));
		this.setHumedad(arrayDatosMeteo.get(10));
		this.setTemp(arrayDatosMeteo.get(11));
		this.setTemp_min(arrayDatosMeteo.get(12));
		this.setTemp_max(arrayDatosMeteo.get(13));

		this.setVelocidad(arrayDatosMeteo.get(14));
		this.setGrado(arrayDatosMeteo.get(15));

		//			this.add(this);

	}



	/*-------------------
	 * Getters & Setters
	 *-------------------*/

	public long getLog() {
		return log;
	}

	public void setLog(long log) {
		this.log = log;
	}

	public long getLat() {
		return lat;
	}

	public void setLat(long lat) {
		this.lat = lat;
	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getPresion() {
		return presion;
	}

	public void setPresion(String presion) {
		this.presion = presion;
	}

	public String getHumedad() {
		return humedad;
	}

	public void setHumedad(String humedad) {
		this.humedad = humedad;
	}

	public String getTemp_min() {
		return temp_min;
	}

	public void setTemp_min(String temp_min) {
		this.temp_min = temp_min;
	}

	public String getTemp_max() {
		return temp_max;
	}

	public void setTemp_max(String temp_max) {
		this.temp_max = temp_max;
	}

	public String getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(String velocidad) {
		this.velocidad = velocidad;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
		setTextNota();
	}

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNomLocalidad() {
		return nombLocalidad;
	}

	public void setNomLocalidad(String nombre) {
		this.nombLocalidad = nombre; 
	}
	public String getNomPaisCiudad() {
		return nombCiudadPais;
	}

	public void setNomCiudadPais(String nombre) {
		this.nombCiudadPais = nombre; 
	}


	public String getNota() {
		return nota;
	}


	public void setNota(String nota) {
		this.nota = nota;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getIcon() {
		return icon;
	}


	public void setIcon(String icon) {
		this.icon = icon;
	}


	public String getTextNota() {
		this.setTextNota();
		return textNota;
	}


	public void setTextNota(String textNota) {
		this.textNota = textNota;
	}

	public void setTextNota() {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();

		pairs.add(new BasicNameValuePair("id",   Long.toString(this.id)));
		pairs.add(new BasicNameValuePair("nombre",  this.nombLocalidad));
		pairs.add(new BasicNameValuePair("nombre2",  this.nombCiudadPais));
		pairs.add(new BasicNameValuePair("main",  this.main));
		pairs.add(new BasicNameValuePair("icon",  this.icon));
		pairs.add(new BasicNameValuePair("descripcion",  this.descripcion));
		pairs.add(new BasicNameValuePair("fecha",  this.fecha));

		pairs.add(new BasicNameValuePair("lat",   Long.toString(this.lat)));
		pairs.add(new BasicNameValuePair("log",   Long.toString(this.log)));

		pairs.add(new BasicNameValuePair("presion",  this.presion));
		pairs.add(new BasicNameValuePair("humedad",  this.humedad));
		pairs.add(new BasicNameValuePair("temp",  this.temp));
		pairs.add(new BasicNameValuePair("temp_min",  this.temp_min));
		pairs.add(new BasicNameValuePair("temp_max",  this.temp_max));

		pairs.add(new BasicNameValuePair("velocidad",  this.velocidad));
		pairs.add(new BasicNameValuePair("grado",  this.grado));
		pairs.add(new BasicNameValuePair("void",  ""));


		this.textNota = pairs.toString();

		this.textNota = textNota;
	}

	/*----------------------------------------------
	 * Metodos Parcelable
	 *----------------------------------------------*/
	public static final Parcelable.Creator<DatosMeteo> CREATOR = new Creator<DatosMeteo>() {

		@Override
		public DatosMeteo createFromParcel(Parcel source) {
			long log = source.readLong();
			long lat = source.readLong();

			// Varios
			String temp = source.readString();
			String presion = source.readString(); 
			String humedad = source.readString(); 
			String temp_min = source.readString(); 
			String temp_max = source.readString();

			//Viento
			String velocidad = source.readString(); 
			String grado = source.readString();

			//General
			long id = source.readLong();
			String main = source.readString();
			String descripcion  = source.readString();
			//String img = source.readString();


			String nombre = source.readString();
			String nombre2 = source.readString();
			String icon = source.readString();
			String fecha = source.readString();
			String textNota = source.readString();

			return new DatosMeteo(log,lat,temp,presion,humedad,
					temp_min,temp_max,velocidad,grado,id,main,descripcion,nombre,nombre2,icon,fecha,textNota);
		}

		@Override
		public DatosMeteo[] newArray(int size) {
			return new DatosMeteo[size];
		}

	};


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(this.log);
		dest.writeLong(this.lat);

		// Varios
		dest.writeString(this.temp);
		dest.writeString(this.presion);
		dest.writeString(this.humedad);
		dest.writeString(this.temp_min);
		dest.writeString(this.temp_max);

		//Viento
		dest.writeString(this.velocidad);
		dest.writeString(this.grado);

		//General
		dest.writeLong(this.id);
		dest.writeString(this.main);
		dest.writeString(this.descripcion);
		//dest.writeString(this.img);

		dest.writeString(this.nombLocalidad);
		dest.writeString(this.nombCiudadPais);
		dest.writeString(this.icon);
		dest.writeString(this.fecha);
		dest.writeString(this.textNota);

	}

}
