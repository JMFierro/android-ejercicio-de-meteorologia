package com.jmfierro.utad.meteo.Datos;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.jmfierro.utad.meteo.Utils.Utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;



public class DatosMeteoList implements Parcelable{

	private ArrayList<DatosMeteo> mDatosMeteoList;
	private TreeMap<Long, DatosMeteo> byId; // Para buscar en el array

	public static final Parcelable.Creator<DatosMeteoList> CREATOR = new Creator<DatosMeteoList>() {

		@Override
		public DatosMeteoList createFromParcel(Parcel source) {
			DatosMeteoList datosMeteoList = new DatosMeteoList();
			Parcelable[] datosMeteoArray = source
					.readParcelableArray( DatosMeteoList.class.getClassLoader() );
			if (datosMeteoList != null) {
				for (Parcelable datosMeteo : datosMeteoArray) {
					datosMeteoList.add((DatosMeteo) datosMeteo);
				}
			}

			return datosMeteoList;
		}

		@Override
		public DatosMeteoList[] newArray(int size) {
			return new DatosMeteoList[size];
		}

	};

	//Constructor
	public DatosMeteoList() {
		mDatosMeteoList= new ArrayList<DatosMeteo>();
		byId = new TreeMap<Long, DatosMeteo>();
	}


	/**==========================
	 * Constructor 
	 * ... de la base de datos.
	 *===========================
	 * @throws JSONException */
	public DatosMeteoList(List<Nota> listaNotas) {
		this();



		Nota[] array2 = new Nota[listaNotas.size()];
		//		for(int i = 0; i < listaNotas.size(); i++) array2[i] = listaNotas.get(i);
		for(int i = 0; i < listaNotas.size(); i++) {

			this.add(new DatosMeteo(listaNotas.get(i).gettexto(), listaNotas.get(i).getId()));

		}	
	}



	/**================================
	 * De JSON Object a DatosMeteoList
	 *=================================*/
	public DatosMeteoList (InputStream streamJSON) {
		this();

		String stringJSONBusqueda = Utils.stringJSONfromStream(streamJSON);

		try {


			JSONObject jsonObject = new JSONObject(stringJSONBusqueda);

			if (jsonObject.getString("message").equals("accurate")) {
				JSONArray localidades = jsonObject.getJSONArray("list"); 

				for (int i=0; i < localidades.length(); i++) {
					JSONObject objMain = localidades.getJSONObject(i);



					/*
					 *  Nombre, pais y coordenadas
					 */
					DatosMeteo datosMeteo = new DatosMeteo();

					String nombre = objMain.getString("name");

					JSONObject objJSON = objMain.getJSONObject("sys");
					nombre = nombre + ", " + objJSON.getString("country");

					objJSON = objMain.getJSONObject("coord");
					long lat = objJSON.getLong("lat");
					datosMeteo.setLat(lat);
					long log = objJSON.getLong("lon");
					datosMeteo.setLog(log);

					datosMeteo.setNomLocalidad(nombre);

					this.add(datosMeteo);
				}

			}
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}
	}	



	public DatosMeteoList (String stringJSONBusqueda) {
		this();

		try {


			JSONObject jsonObject = new JSONObject(stringJSONBusqueda);

			if (jsonObject.getString("message").equals("accurate")) {
				JSONArray localidades = jsonObject.getJSONArray("list"); 

				for (int i=0; i < localidades.length(); i++) {
					JSONObject objMain = localidades.getJSONObject(i);



					/*
					 *  Nombre, pais y coordenadas
					 */
					DatosMeteo datosMeteo = new DatosMeteo();

					String nombre = objMain.getString("name");

					JSONObject objJSON = objMain.getJSONObject("sys");
					nombre = nombre + ", " + objJSON.getString("country");

					objJSON = objMain.getJSONObject("coord");
					long lat = objJSON.getLong("lat");
					datosMeteo.setLat(lat);
					long log = objJSON.getLong("lon");
					datosMeteo.setLog(log);


					datosMeteo.setNomLocalidad(nombre);

					this.add(datosMeteo);
				}

			}
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

	}



	/**================================
	 * De JSON Object a DatosMeteoList
	 *=================================*/
	static public DatosMeteoList parseJSONBusqueda(InputStream streamJSON) {

		String stringJSON = Utils.stringJSONfromStream(streamJSON);
		//DatosMeteoList datosMeteoList = parseJSONBusqueda(stringJSON);
		//DatosMeteoList datosMeteoList = new DatosMeteoList(stringJSON);
		DatosMeteoList datosMeteoList = new DatosMeteoList();
		datosMeteoList = DatosMeteoList.parseJSONBusqueda(stringJSON);

		return datosMeteoList;
	}	

	static public DatosMeteoList parseJSONBusqueda(String stringJSON) {


		DatosMeteoList datosMeteoList = new DatosMeteoList();
		try {

			JSONObject jsonObject = new JSONObject(stringJSON);

			if (jsonObject.getString("status").equals("OK")) {
				JSONArray localidades = jsonObject.getJSONArray("results"); 

				for (int i=0; i < localidades.length(); i++) {
					JSONObject objMain = localidades.getJSONObject(i);


					String nombre = objMain.getString("formatted_address");
					//					TextView textLocalidad = (TextView) view.findViewById(R.id.text_Item_ListaLocalidades_Nomb);
					//					textLocalidad.setText(nombre);
					DatosMeteo datosMeteo = new DatosMeteo();
					datosMeteo.setNomLocalidad(nombre);

					JSONObject objGeo = objMain.getJSONObject("geometry");
					JSONObject objLoc = objGeo.getJSONObject("location");
					long lat = objLoc.getLong("lat");
					datosMeteo.setLat(lat);
					long log = objLoc.getLong("lng");
					datosMeteo.setLog(log);

					datosMeteoList.add(datosMeteo); 
				}

			}
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}
		return datosMeteoList;
	}


	public void add(DatosMeteo datosMeteo) {
		if (datosMeteo != null)
			add(new DatosMeteo[]{ datosMeteo });
	}

	private void add(DatosMeteo... datosMeteoArray) { //recibe array de datosMeteo
		// Convierte un array en una lista
		this.mDatosMeteoList.addAll(Arrays.asList(datosMeteoArray));

		for (DatosMeteo datosMeteo : datosMeteoArray) {
			long id = datosMeteo.getId();
			this.byId.put(datosMeteo.getId(), datosMeteo);
		}
	}


	public DatosMeteo get(long id) {  // Devuelve el datosMeteo
//		if (byId.size() == 0)
//			return null;
			
		return byId.get(id);
	}


	public int getPosition(long id) {
		DatosMeteo datosMeteo = get( id );
		return mDatosMeteoList.indexOf( datosMeteo );

	}



	public ArrayList<DatosMeteo> getdatosMeteoList() {
		return mDatosMeteoList;
	}



	@Override
	public int describeContents() {
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		DatosMeteo[] datosMeteoArray = this.mDatosMeteoList.toArray( new DatosMeteo[0] );
		dest.writeParcelableArray(datosMeteoArray, 0);
	}

	public int getSize () {
		return mDatosMeteoList.size();
	}


} // Fin clase DatosMeteoList
