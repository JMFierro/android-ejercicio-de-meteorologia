package com.jmfierro.utad.meteo.Web;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;


public class NetworkStatusReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
    	Log.i(NetworkStatusReceiver.class.getSimpleName(), "*****************************************");
    	Log.i(NetworkStatusReceiver.class.getSimpleName(), "Se ha ejecutado el NetworkStatusReceiver!!");
    	Log.i(NetworkStatusReceiver.class.getSimpleName(), "*****************************************");
		
		// Esta comprobación es redundante con el intent-filter, pero es conveniente hacerla
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
        	
        	// Obtenemos el estado de la red
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            
            if (noConnectivity) {
            	Log.d(NetworkStatusReceiver.class.getSimpleName(), "No hay conectividad");
        		WebServicio.cancelProgramService(context);
            } else {
            	Log.d(NetworkStatusReceiver.class.getSimpleName(), "Sí hay conectividad");
        		WebServicio.programService(context);
        	} 
        }
	}
}