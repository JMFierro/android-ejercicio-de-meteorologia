package com.jmfierro.utad.meteo.Web;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.jmfierro.utad.meteo.R;


/**
 * Este tipo de receiver (BOOT_COMPLETED) puede ser muy útil para inicializar nuestras aplicaciones
 * cuando el dispositivo se inicia. Se puede utilizar para:
 * 
 *  - Inicializar algún servicio que nuestra app pueda necesitar.
 *  - Programar las alarmas futuras.
 *  - Realizar peticiones de red para comprobar el estado de la app en algún servidor.
 * 
 */ 
public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		
		Log.i(BootReceiver.class.getSimpleName(), "********************************");
		Log.i(BootReceiver.class.getSimpleName(), "Se ha ejecutado el BootReceiver!!");
		Log.i(BootReceiver.class.getSimpleName(), "********************************");
		
		// Esta comprobación es redundante con el intent-filter, pero es conveniente hacerla
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Toast.makeText(context, R.string.aviso_intervalos_alarma, Toast.LENGTH_LONG).show();

			// Programamos el servicio
			WebServicio.programService(context);
			
        }
	}
}