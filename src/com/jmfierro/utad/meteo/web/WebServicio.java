package com.jmfierro.utad.meteo.Web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;


import com.jmfierro.utad.meteo.R;
import com.jmfierro.utad.meteo.ActivPlus.DetalleLocalidadFragmento;
import com.jmfierro.utad.meteo.ActivPlus.ListaLocalidadesFragmento;
import com.jmfierro.utad.meteo.ActivPlus.BaseActionBarActivity;
import com.jmfierro.utad.meteo.Actividades.MainActivity;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Datos.DatosMeteoList;
import com.jmfierro.utad.meteo.Datos.Nota;
import com.jmfierro.utad.meteo.SQL.NotasDataSource;
import com.jmfierro.utad.meteo.Utils.Utils;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.ListView;

public class WebServicio extends Service { 

	public static final int SERVICE_REQUEST_CODE = 11;
	public static final int WHEN_MILLIS = 10000; // 10 segundos
//	public static final int REPEATING_MILLIS = 30000; // 30 segundos

	
	public static final int ACTIVITY_REQUEST_CODE = 20;
	public static final int MY_NOTIFICATION_ID = 30;
	// Constantes para las alarmas
	public static final String LOADED_ACTION = "com.jmfierro.utad.meteo.Actividades.LOADED_ACTION";
	public static final String LOADED_ACTION_DATOSMETEOLIST = "datosMeteoList";

	
	private String 	mTitle;

	public static boolean isActualizandoseSQLfromWebTask = false;
	
//	private boolean isSQLAtualizandose = false;
	
	

	/*---------------------------------------------------------
	 * Métodos para callbacks entre Actividade cliente y Servicio:
	 *---------------------------------------------------------*/


//	/*
//	 * >> Método callback para la Actividad cliente.
//	 */
//	public interface OnWebServicioListener {
//		//		public void onSetDatosMeteo(DatosMeteoList datosMeteoList);
//		public void onSetDatosMeteoListFromSQL();
////		public DatosMeteo parseJSON(DatosMeteo datosMeteo,String stringJSON);
////		public DatosMeteo parseJSON(String stringJSON);
//
//	}
//
//	public void setOnWebServicioListener (OnWebServicioListener aWebListener) {
//		this.mWebListener = aWebListener;
//	}

	
	
	
	/* 
	 * >> Binder para recibir llamadas de las Actividades cliente.
	 *
	 *
	 *  >> Clase usada por la Actividad cliente para comunicarse con el servicio 
	 *  (Actividad -> Servicio), devuelve el objeto servicio.
	 *  
	 * (El método onBind() debe devolver una referencia a este metodo para permitir la conexión al servicio.)
	 */
	public class WebBinder extends Binder {
		public WebServicio getService(){ 
			return WebServicio.this;
		}
	}




	//	/*----------------------------------------------------
	//	 * Método al que acceden los Clientes desde su proceso
	//	 *----------------------------------------------------*/
	//	public void loadWebDatosMeteo() {
	//		Log.d(WebServicio.class.getSimpleName(),"Descarga y actualización de datos");
	//
	//		/*
	//		 * Acceso a la web en un hilo separado 
	//		 */
	////		new LoadWedTask().execute("http://api.openweathermap.org/data/2.5/weather?q=Madrid");
	////		new LoadWedTask().execute(40.6,-3.6);
	//		loadWebTask bl = new loadWebTask() {
	//
	//			@Override
	//			public InputStream loadRaw() {
	//				return getApplication().getResources()
	//				         .openRawResource(R.raw.openweathermap_weather_madrid);
	//			}
	//
	//			@Override
	//			public Object parseJSON(String stringJSON) {
	//				/*------------------------------
	//				 * Parse objeto JSON
	//				 *------------------------------*/
	//				DatosMeteo datosMeteo = new DatosMeteo();
	//				if (mWebListener != null)
	//					datosMeteo = WebServicio.this.mWebListener.parseJSON(stringJSON);
	//				return datosMeteo;
	//			}
	//
	//			@Override
	//			public void onSetDatos(Object object) {
	//				if (object != null)
	//					mDatosMeteoList.add((DatosMeteo) object);
	//			}
	//
	//			@Override
	//			public void updateMyView(Object object) {
	//				DatosMeteoList datosMeteoList = new DatosMeteoList();
	//				datosMeteoList.add((DatosMeteo) object);
	//				if (WebServicio.this.mWebListener != null)
	//					WebServicio.this.mWebListener.onSetDatosMeteo(datosMeteoList);
	//				
	//			}
	//			
	//		};
	//
	//		/*----------------
	//1		 * Lanza hilo
	//		 *----------------*/
	////		DatosMeteoList d  = new DatosMeteoList(); 
	////		d = MeteoListaLocalidadesFragmento.getArrayListDatosMeteo();
	//
	////		bl.execute(40.6,-3.6);
	//		bl.execute(Config.mLatitud,Config.mLongitud);
	//		
	////		if (WebServicio.this.mWebListener != null)
	////			WebServicio.this.mWebListener.onSetDatosMeteo(mDatosMeteoList);
	//		
	//	}



	/*--------------------------------------------------------------
	 * Sobrescritura de métodos de la clase Service para binder:
	 *--------------------------------------------------------------*/

	/*
	 * >> Recibe la peticiones de conexión al servicio por parte de clientes.
	 * 	  Debe devolver un IBinder para que se establezca la conexión.
	 */
	@Override
	public IBinder onBind(Intent arg0) { 
		Log.d(WebServicio.class.getSimpleName(), "onBind");
		
		/*
		 * No me hace falta constructor para guardar en una 
		 * variable el bind, lo creo y envio directamente.
		 */
		return new WebBinder();
	}



	@Override
	public void onCreate() {
		Log.d(WebServicio.class.getSimpleName(), "onCreate");

		super.onCreate();
	}
	
	
	/*----------------------------------------------------------------------------------------------
	 * Si el servicio es llamado desde el startService() onStar() y onStarCommand() son llamados.
	 * Lee de la red y lo guarda en base de datos. 
	 *-----------------------------------------------------------------------------------------------*/
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(WebServicio.class.getSimpleName(),"+++++++++++++++++");
		Log.d(WebServicio.class.getSimpleName(),"onStartCommand");
		Log.d(WebServicio.class.getSimpleName(),"+++++++++++++++++");

		//new UrlToSQLAsyncTask().execute("http://www.dumblittleman.com/feeds/posts/default?alt=json");
		Log.d(WebServicio.class.getSimpleName(),"Llamada a actualizarSQLfromWebTask()");
		
		actualizarSQLfromWeb();
		
//		if (Utils.isNet())
//			new actualizarSQLfromWebTask().execute();
//		else
//			Log.d(WebServicio.class.getSimpleName(),"No hay red!: No se actualizan los datos");
			

		return START_STICKY;    // Indica que el servicio se reabra (sin intent) si ha sido destruido por el sistema.
	}


	/*
	 * >> Finalización del Servicio.
	 */
	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(WebServicio.class.getSimpleName(), "onUnbind");
//		this.mWebListener = null;
		return super.onUnbind(intent);
	}

	
	@Override
	public void onDestroy() {
		Log.d(WebServicio.class.getSimpleName(), "onDestroy");
		super.onDestroy();
	}
	
	
	public boolean actualizarSQLfromWeb() {
		Log.d(WebServicio.class.getSimpleName(), "actualizarSQLfromWeb()");
		// Comprobamos si hay conectividad

		if (!isActualizandoseSQLfromWebTask) {
			if (Utils.isNet() || MainActivity.LECTURA_DESDE_RAW) {
				new actualizarSQLfromWebTask().execute();

				return true;
			}
			else {
				Log.d(WebServicio.class.getSimpleName(), "No hay conexión a internet!: no se actuliza la base de datos");
				notifyUser(getString(R.string.notification_msg_no_conexion));

				//			/*
				//			 * >> Aviso al cliente cuuando la base de datos esta acutalizada.
				//			 */
				//			if (WebServicio.this.mWebListener != null)
				//				WebServicio.this.mWebListener.onSetDatosMeteoListFromSQL(); 

				return false;
			}
		}
		return false;
	}

	
	/**==================================================================
	 * Tarea en segundo plano para realizar la descarga de internet
	 * ==================================================================*/
	public class actualizarSQLfromWebTask extends AsyncTask<Void, Void, DatosMeteoList> {
		//		public abstract class loadWebTask extends AsyncTask<Double, Void, Object> {


		
		@Override
		protected void onPreExecute() {

			isActualizandoseSQLfromWebTask = true;
			
			mTitle = (String) MainActivity.mMainActivityThis.getTitle();
			MainActivity.mMainActivityThis.setTitle(getString(R.string.actualizandose_db));
			
//			  itemEliminarMenuItem.setEnabled(false);
//		    if (myItemShouldBeEnabled) {
//		        item.setEnabled(true);
//		        item.getIcon().setAlpha(255);
//		
			
			
			super.onPreExecute();
		}

		protected DatosMeteoList doInBackground(Void... params) {

			Log.d(WebServicio.class.getSimpleName(), "Actualizando base de datos SQL");
			
			/*
			 * Abre base de datos
			 */
			// SQLite
			ListView lvNotas;
			NotasDataSource dataSource = null;
			dataSource = new NotasDataSource(MainActivity.mMainActivityThis); //this);
			dataSource.open();
			List<Nota> listaNotas = dataSource.getAllNotas();
			
			long lat,log;

			InputStream streamJSONitem;
			String stringJSONitem = null;
			DatosMeteoList datosMeteoListFromNota = new DatosMeteoList(listaNotas);
			DatosMeteoList datosMeteoListActualizado = new DatosMeteoList();
			for(int i = 0; i < listaNotas.size(); i++) { 

				long idOfListaNotas = listaNotas.get(i).getId();
				DatosMeteo datosMeteoOld = new DatosMeteo(listaNotas.get(i).gettexto(),idOfListaNotas);

				
				/*
				 * Baja datos de red
				 */
				lat = datosMeteoListFromNota.getdatosMeteoList().get(i).getLat();
				log = datosMeteoListFromNota.getdatosMeteoList().get(i).getLog();
				if (Utils.isNet()) {
					stringJSONitem = Utils.downloadUrl(lat,log);
				}
				else if (MainActivity.LECTURA_DESDE_RAW) {
					streamJSONitem = getApplication().getResources()
							.openRawResource(R.raw.openweathermap_weather_madrid);
					stringJSONitem = Utils.stringJSONfromStream(streamJSONitem);
				}

				String nombPaisCiudad = datosMeteoOld.getNomPaisCiudad();
				DatosMeteo datosMeteoActualizado = new DatosMeteo(stringJSONitem, nombPaisCiudad);
				datosMeteoListActualizado.add(datosMeteoActualizado);
				
				
				/*
				 * Actualiza base de datos
				 */
				Nota nota = dataSource.getNotas(idOfListaNotas);
//				String textNotaNuevaString = datosMeteoNew.getTextNota();
	
//				datosMeteoOld.setFecha(Utils.getFechaActual());
				
				/*
				 * ** Puebas **
				 */
				if (MainActivity.PRUEBA) {
					datosMeteoOld.setFecha("17/17/");
					nota.setTexto(datosMeteoOld.getTextNota()); 
				}
				else
					nota.setTexto(datosMeteoActualizado.getTextNota());
	
				
				Nota nota2 = dataSource.getNotas(nota.getId());

				dataSource.borrarNota(nota);
				dataSource.crearNota(nota.gettexto());

				nota = dataSource.getNotas(nota.gettexto());
				datosMeteoActualizado.setId(nota.getId());
			
			}


			dataSource.close(); 

//			List<Nota> listaNotas2 = NotasDataSource.getAllNotasOpenClose(); 

			
			return datosMeteoListActualizado;

		}

		
		
		/**===========================
		 * Finalización del hilo. 
		 * Aviso callbacks al cliente
		 *===========================*/
		public void onPostExecute(DatosMeteoList result) {
			Log.d(WebServicio.class.getSimpleName(),"onPostExecute(): Finaliza la actualización de SQL");


//			/*
//			 * >> Aviso al cliente cuuando la base de datos esta acutalizada.
//			 */
//			if (WebServicio.this.mWebListener != null)
//				WebServicio.this.mWebListener.onSetDatosMeteoListFromSQL();
			
			// Envía un broadcast
			Log.d(WebServicio.class.getSimpleName(),"Envia broadcast LOADER_ACTION");
			Intent broadcastMessage = new Intent();
			broadcastMessage.putExtra(LOADED_ACTION_DATOSMETEOLIST,(DatosMeteoList) result);
			broadcastMessage.setAction(LOADED_ACTION);
			sendBroadcast(broadcastMessage);

			// Crea la notificación para el usuario
			notifyUser(getString(R.string.notification_msg_sql_actualizada));

			MainActivity.mMainActivityThis.setTitle(mTitle);
			
			isActualizandoseSQLfromWebTask = false;
		}

	}
	

	
	
	
	public static PendingIntent getPendingIntent(Context context) {
		// Creamos el intent
		Intent myIntent = new Intent(context, WebBinder.class);
		
		// Creamos el pending intent
		PendingIntent pendingIntent = PendingIntent.getService(
				context, 
				SERVICE_REQUEST_CODE, 
				myIntent, 
				PendingIntent.FLAG_CANCEL_CURRENT);
		 
		// Devolvemos el pendingIntent que representa el servicio
		return pendingIntent;
	}
	
	public static void programService(Context context) {
		
		// Lee las preferencias del usuario.
		String repeatingPreference = PreferenceManager.getDefaultSharedPreferences(context)
				.getString(
						"sync_frequency", 
						"" + context.getResources().getInteger(R.integer.pref_sync_frequency_default));
		
		int repeatingMillis = Integer.parseInt(repeatingPreference) * 60 * 1000; // minutes to milliseconds

		programService(context, repeatingMillis);
	}
	
	public static void programService(Context context, int repeatingMillis) {
		
		Log.d(WebServicio.class.getSimpleName(), "Download de datos meteorológicos cada " + repeatingMillis/1000 + " s.");
		
		// Creamos el PendingIntent
		PendingIntent pendingIntent = WebServicio.getPendingIntent(context);
		
		// Creamos una alarma que se repite periódicamente
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		am.setInexactRepeating(AlarmManager.RTC_WAKEUP, 
				System.currentTimeMillis() + WHEN_MILLIS, // Dentro de 10 segundos
				repeatingMillis,
				pendingIntent);
	}
	
	public static void cancelProgramService(Context context) {
		// Creamos el PendingIntent
		PendingIntent pendingIntent = WebServicio.getPendingIntent(context);
		
		// Cancelamos la alarma
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		am.cancel(pendingIntent);
		
	}

	
	
	/*
	 * Notificacion del servicio
	*/
	private void notifyUser() {
		
		notifyUser(getText(R.string.notification_description).toString());
		
	}
	
	
	private void notifyUser(String msg) {

		// Se crea el Intent
		Intent myIntent = new Intent(this, MainActivity.class);
		myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		// Se almacena el Intent en el PendingIntent
		PendingIntent myPendingIntent = PendingIntent.getActivity(
				this,
				ACTIVITY_REQUEST_CODE,
				myIntent,
				Intent.FLAG_ACTIVITY_NEW_TASK); 
		
		// Programamos una notificación (con el icono de la aplicación) 
		// que cuando se pulse se abra la aplicación
		Notification notification = new NotificationCompat.Builder(this)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.meteorologia_nube1))
				.setSmallIcon(android.R.drawable.ic_menu_recent_history)
				.setContentTitle(getText(R.string.notification_title))
				.setContentText(msg)
//				.setContentText(getText(R.string.notification_description))
				.setContentIntent(myPendingIntent)
				.setAutoCancel(true)
				.build();
		
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(MY_NOTIFICATION_ID, notification);

	}
	
	
}