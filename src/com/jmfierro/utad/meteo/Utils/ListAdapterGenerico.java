package com.jmfierro.utad.meteo.Utils;

import java.util.ArrayList;

import com.jmfierro.utad.meteo.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class ListAdapterGenerico extends BaseAdapter {

	private ArrayList<?> entradas; 
	private int mR_layout_IdView; 
	private Context mMeteoContext;


	/**=======================================================================================================
	 *  Devuelve las vistas de cada item.
	 *  
	 * @param aItem El item que será la asociada a la view. La entrada es del tipo del paquete/handler
	 * @param view View particular que contendrá los datos del paquete/handler
	 *=========================================================================================================*/
	public abstract void onMyItemView (Object entrada, View view);

	/**==============================================================
	 * Constructor: guarda la referencia a la actividad principal
	 * 				como contexto 
	 *===============================================================*/
	public ListAdapterGenerico(Context contexto, int R_layout_IdView, ArrayList<?> entradas) {
		super();
		this.mMeteoContext = contexto;
		this.entradas = entradas; 
		this.mR_layout_IdView = R_layout_IdView; 
	}



	@Override
	public int getCount() {
		return entradas.size();
	}

	@Override
	public Object getItem(int posicion) {
		return entradas.get(posicion);
	}

	@Override
	public long getItemId(int posicion) {
		return posicion;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		/*-------------------------------------
		 *  Si no existe se crea la vista
		 *-------------------------------------*/
		if(convertView == null) {

			LayoutInflater inflater = (LayoutInflater) mMeteoContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(mR_layout_IdView, null);
		}

		/*------------------------------------------------------------------
		 * Rellena la vista del item en el método que utiliza el adaptador.
		 *------------------------------------------------------------------*/
		onMyItemView (entradas.get(position), convertView);


		return convertView;
	}

}
