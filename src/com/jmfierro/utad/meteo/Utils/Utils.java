package com.jmfierro.utad.meteo.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat; 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.jmfierro.utad.meteo.R;
import com.jmfierro.utad.meteo.ActivPlus.ListaLocalidadesFragmento;
import com.jmfierro.utad.meteo.Actividades.MainActivity;
import com.jmfierro.utad.meteo.Datos.DatosMeteo;
import com.jmfierro.utad.meteo.Web.loadWebTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;


public class Utils {

	// flags para downloadUrl() de www.openweathermap.org
	public static final String CONSULTA_WEATHER = "weather";
	public static final String CONSULTA_FIND = "find";
	public static final String CONSULTA_DAILY = "daily";
	public static final String CONSULTA_CITY = "city";

	public Utils (){
	}


	/**=======================================
	 * Comprueba de hay conexión de internet.	
	 * ====================================== */
	static public boolean isNet() {
		// Gets the URL from the UI's text field.
		//String stringUrl = urlText.getText().toString();
		//        ConnectivityManager connMgr = 
		//        		(ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		ConnectivityManager connMgr = 
				(ConnectivityManager) MainActivity.mMainActivityThis.getSystemService(Context.CONNECTIVITY_SERVICE);
		//        ConnectivityManager connMgr = 
		//        		(ConnectivityManager) Config.mContextMain.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected() 
				&& networkInfo.isAvailable() 
				&& networkInfo.isConnectedOrConnecting()) {
			boolean isAvailable = networkInfo.isAvailable();
			boolean isConnectedOrConnecting = networkInfo.isConnectedOrConnecting();
			boolean faivoler = networkInfo.isFailover();
			boolean romaing = networkInfo.isRoaming();
			State state = networkInfo.getState();

			//new DownloadWebpageTask().execute(stringUrl);
			return true;
		} else {
			return false;
		}

	}


	/**====================
	 *  De Stream a String
	 *=====================*/
	static public String stringJSONfromStream(InputStream streamJSON) {

		BufferedReader buffReader = null;
		buffReader = new BufferedReader(new InputStreamReader(streamJSON,Charset.forName("UTF-8")));

		StringBuffer buffer = new StringBuffer();
		String s = null;
		try {
			while((s = buffReader.readLine()) != null) {
				buffer.append(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		String stringJSON = buffer.toString();
		return stringJSON;
	}

	/**===============================
	 * Fecha actual del sistema
	 * @return
	 *================================*/
	static public String getFechaActual() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy\nHH:mm:ss");
		//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return sdf.format(new Date());
	}


	/**===========================================================
	 * Descarga datos de la web indicando nombre de la localidad
	 *============================================================*/
	/* 
	 * OPCIONES :
	 * ----------
	 * maps.googleapis.com/maps/api/geocode/json?address=leon&sensor=false 
	 * http://api.openweathermap.org/data/2.5/weather
	 * http://api.openweathermap.org/data/2.5/find?q=Leon&mode=json
	 * http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&lang=sp				
	 * http://openweathermap.org/data/2.3/forecast/city?id=524901&APPID=1111111111
	 * 
	 * 
	 * 	// flags para downloadUrl()
	 *	public static final String CONSULTA_WEATHER = "weather";
	 *	public static final String CONSULTA_FIND = "find";
	 *	public static final String CONSULTA_DAILY = "daily";
	 *	public static final String CONSULTA_CITY = "city";
	 *
	 * 
	 *  English - en, Russian - ru, Italian - it, Spanish - sp, Ukrainian - ua, German - de, Portuguese - pt, 
	 *  Romanian - ro, Polish - pl, Finnish - fi, Dutch - nl, French - fr, Bulgarian - bg, Swedish - se, 
	 *  Chinese Traditional - zh_tw, Chinese Simplified - zh_cn, Turkish - tr 
	 *  
	 *   * the measured wind direction (in degrees, 0°=N, 90°=E, 180°=S, 270°=W)

	 */
	static public String downloadUrl(String stringLocalidad,String tipoCounsulta) {

		/*
		 *  Crear una nueva lista de pares valor/clave para utilizar como parámetros en la solicitud
		 */
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();


		/*
		 * Parametros para el query.
		 */
		pairs.add(new BasicNameValuePair("APPID",   "AIzaSyDlwLnt1rTsG1f5GAIv1y0bdN04oS-3i4M"));
		pairs.add(new BasicNameValuePair("q",  stringLocalidad));
		pairs.add(new BasicNameValuePair("lang",  "sp"));
		pairs.add(new BasicNameValuePair("units",  "metric"));
		pairs.add(new BasicNameValuePair("mode",  "json"));

		//		String s = pairs.get(1).getValue();

		// Create a nueva URL
		String stringUrlOpenweathermapLocalidad = null; 
		if (tipoCounsulta == CONSULTA_WEATHER) {
			stringUrlOpenweathermapLocalidad = "http://api.openweathermap.org/data/2.5/weather";
		} else if (tipoCounsulta == CONSULTA_FIND) {
			stringUrlOpenweathermapLocalidad = "http://api.openweathermap.org/data/2.5/find";
		}

		stringUrlOpenweathermapLocalidad = stringUrlOpenweathermapLocalidad + 
				"?"+ URLEncodedUtils.format(pairs,"utf-8");

		//		String stringUrlOpenweathermapLocalidad = 
		//				"http://api.openweathermap.org/data/2.5/weather" +"?"+ URLEncodedUtils.format(pairs,"utf-8");
		//		Log.d(loadWebTask.class.getSimpleName(),stringUrlOpenweathermapLocalidad);

		String stringSON = downloadUrlconexion(stringUrlOpenweathermapLocalidad);

		return stringSON;
	}

	/**==============================================
	 * Descarga datos de la web indicando  longitud
	 *===============================================*/
	/* 
	 * OPCIONES :
	 * ----------
	 * maps.googleapis.com/maps/api/geocode/json?address=leon&sensor=false 
	 * http://api.openweathermap.org/data/2.5/weather
	 * http://api.openweathermap.org/data/2.5/find?q=Leon&mode=json
	 * http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&lang=sp				
	 * http://openweathermap.org/data/2.3/forecast/city?id=524901&APPID=1111111111
	 * 
	 * 	// flags para downloadUrl()
	 *	public static final String CONSULTA_WEATHER = "weather";
	 *	public static final String CONSULTA_FIND = "find";
	 *	public static final String CONSULTA_DAILY = "daily";
	 *	public static final String CONSULTA_CITY = "city";
	 *
	 *  English - en, Russian - ru, Italian - it, Spanish - sp, Ukrainian - ua, German - de, Portuguese - pt, 
	 *  Romanian - ro, Polish - pl, Finnish - fi, Dutch - nl, French - fr, Bulgarian - bg, Swedish - se, 
	 *  Chinese Traditional - zh_tw, Chinese Simplified - zh_cn, Turkish - tr 
	 *  
	 *   * the measured wind direction (in degrees, 0°=N, 90°=E, 180°=S, 270°=W)

	 *  
	 */
	static public String downloadUrl(double lat, double log) {

		if (lat==0 && log==0){
			return null;
		}


		/*
		 *  Crear una nueva lista de pares valor/clave para utilizar como parámetros en la solicitud
		 */
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();


		/*
		 * Parametros para el query.
		 */
		pairs.add(new BasicNameValuePair("APPID",   "AIzaSyDlwLnt1rTsG1f5GAIv1y0bdN04oS-3i4M"));
		pairs.add(new BasicNameValuePair("lat",  String.valueOf(lat) ));
		pairs.add(new BasicNameValuePair("lon",   String.valueOf(log)));
		pairs.add(new BasicNameValuePair("lang",  "sp"));
		pairs.add(new BasicNameValuePair("units",  "metric"));

		// Create a nueva URL
		String stringUrlOpenweathermapCoordenadas = 
				"http://api.openweathermap.org/data/2.5/weather" +"?"+ URLEncodedUtils.format(pairs,"utf-8");
		Log.d(loadWebTask.class.getSimpleName(),stringUrlOpenweathermapCoordenadas);

		String stringJSON = downloadUrlconexion(stringUrlOpenweathermapCoordenadas);

		return stringJSON;
	}



	/**================================================================
	 * Descarga datos de la web
	 * Establece conxión y hace la descarga y transformación a string.
	 *=================================================================*/
	/* 
	 * OPCIONES :
	 * ----------
	 * maps.googleapis.com/maps/api/geocode/json?address=leon&sensor=false 
	 * http://api.openweathermap.org/data/2.5/weather
	 * http://api.openweathermap.org/data/2.5/find?q=Leon&mode=json
	 * http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&lang=sp				
	 * http://openweathermap.org/data/2.3/forecast/city?id=524901&APPID=1111111111
	 * 
	 * 
	 *  English - en, Russian - ru, Italian - it, Spanish - sp, Ukrainian - ua, German - de, Portuguese - pt, 
	 *  Romanian - ro, Polish - pl, Finnish - fi, Dutch - nl, French - fr, Bulgarian - bg, Swedish - se, 
	 *  Chinese Traditional - zh_tw, Chinese Simplified - zh_cn, Turkish - tr 
	 *  
	 *   * the measured wind direction (in degrees, 0°=N, 90°=E, 180°=S, 270°=W)

	 */
	static public String downloadUrlconexion(String stringUrl) {
		//ArrayList<DatosMeteo> arrayListDatosMeteo = null;

		String stringJSON = null;
		InputStream streamJSON = null;

		HttpURLConnection con = null;
		try {
			/*-----------------------------------
			 *  Si hay conexión a internet.
			 *----------------------------------*/
			if(Utils.isNet()) {

				//				URL url = new URL("http://api.openweathermap.org/data/2.5/find?q=Leon&mode=json");
				URL url = new URL(stringUrl);

				// Obtiene nueva conexión.
				con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");

				// Set petición para JSON.
				con.setRequestProperty("Accept", "application/json"); 

				//				URL url = new URL("maps.googleapis.com/maps/api/geocode/json?address=leon&sensor=false");
				//		        con = (HttpURLConnection) url.openConnection();
				//		        con.setReadTimeout(15000);
				//		        con.setConnectTimeout(10000);
				//		        con.setRequestMethod("GET");
				//		        con.setDoInput(true);
				//		        
				//		        
				// Starts the query
				con.connect();


				// Precesa la respuesta.
				//reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

				streamJSON = con.getInputStream();
				/*-------------------------
				 *  De Stream a String
				 *-------------------------*/
				BufferedReader buffReader = null;
				buffReader = new BufferedReader(new InputStreamReader(streamJSON,Charset.forName("UTF-8")));

				StringBuffer buffer = new StringBuffer();
				String s = null;
				while((s = buffReader.readLine()) != null) {
					buffer.append(s);
				}

				stringJSON = buffer.toString();
				Log.d("app",stringJSON);
			}

		}
		catch (UnsupportedEncodingException uee) { 
			Log.d("DEBUG", "UnsupportedEncodingException while processing the PUT friend's name");
		}
		catch (IOException ioe) {
			Log.d("DEBUG", "IOException while processing the PUT friend's email");
		} 
		finally {
			// Release connection
			if (con != null)
				con.disconnect();
		}

		return stringJSON;

	} // ** Fin downloadUrl(lat,log,idioma,ud) **




	static public DatosMeteo parseJSONweatherworl (DatosMeteo datosMeteo, String stringJSON) {

		if (stringJSON == null)
			return null;

		datosMeteo = new DatosMeteo();

		/*------------------------------
		 * Parse objeto JSON
		 *------------------------------*/
		try {

			JSONObject jsonObject = new JSONObject(stringJSON);


			datosMeteo.setNomLocalidad(jsonObject.getString("name"));
			datosMeteo.setId(jsonObject.getLong("id"));

			JSONArray array = jsonObject.getJSONArray("weather");

			JSONObject object = array.getJSONObject(0);

			datosMeteo.setMain(object.getString("main"));
			datosMeteo.setDescripcion(object.getString("description"));
			//datosMeteo.setImg(object.getString("icon"));

			JSONObject object2 = jsonObject.getJSONObject("main");
			datosMeteo.setTemp(object2.getString("temp"));
			datosMeteo.setTemp_min(object2.getString("temp_min"));
			datosMeteo.setTemp_max(object2.getString("temp_max"));

			datosMeteo.setPresion(object2.getString("pressure"));
			datosMeteo.setHumedad(object2.getString("humidity"));

			JSONObject object3 = jsonObject.getJSONObject("wind");
			datosMeteo.setVelocidad(object3.getString("speed"));
			datosMeteo.setGrado(object3.getString("deg"));

		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}
		return datosMeteo;


	}






	static public String quitarDecimales (String numeroInString) {

		if (numeroInString == null)
			return "";
		
		String numeroOutString = null;
		int lastDot = numeroInString.lastIndexOf('.');
		if (lastDot == -1) {
			// No dots - what do you want to do?
		} else {
			numeroOutString = numeroInString.substring(0,lastDot);
		}

		if (numeroOutString != null)
			return numeroOutString;
		else
			return numeroInString;

	}
	
	
	//reinicia una Activity
	public static void reiniciarActivity(Activity actividad){
	        Intent intent=new Intent();
	        intent.setClass(actividad, actividad.getClass());
	        //llamamos a la actividad
	        actividad.startActivity(intent);
	        //finalizamos la actividad actual
	        actividad.finish();
	}
	
	
	public static void upHome (Activity thisActivity) {

		Intent intentBack = NavUtils.getParentActivityIntent(thisActivity);

		if (NavUtils.shouldUpRecreateTask(thisActivity, intentBack)) {
			// This activity is NOT part of this app's task, so create a new task
			// when navigating up, with a synthesized back stack.
			TaskStackBuilder.create(thisActivity)
			// Add all of this activity's parents to the back stack
			.addNextIntent(intentBack)
			// Navigate up to the closest parent
			.startActivities();
		}
		else {
			NavUtils.navigateUpTo(thisActivity, intentBack);
		}

	}

//	public void showDialogo(String aTituloString, String aMsgString, boolean aBtnAceptar, boolean aBtnCancelar) {
//
//		int itemSelect2 = ListaLocalidadesFragmento.mItemActual;
//		DatosMeteo datosMeteoSelect2 = ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList().get(itemSelect2);
//
//		AlertDialog.Builder builder2 = new AlertDialog.Builder(this)
//		.setTitle(aTituloString)
////		.setTitle(R.string.borrar_localidad_titulo)
//		.setMessage(aMsgString)
////				.setMessage(datosMeteoSelect2.getNomLocalidad()
////						+ "\n" + datosMeteoSelect2.getNomPaisCiudad())
//				.setPositiveButton(R.string.accept,
//						new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface arg0, int arg1) {
//						eliminarItem();
//					}
//				})
//
//				.setNegativeButton("Cancelar",
//						new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog,
//							int which) {
//						// TODO Auto-generated method stub
//						return;
//					}
//				});
//		builder2.show();
//
//	}
//	
//	public void showDialogo(String aTituloString, String aMsgString, boolean aBtnAceptar) {
//
//		int itemSelect2 = ListaLocalidadesFragmento.mItemActual;
//		DatosMeteo datosMeteoSelect2 = ListaLocalidadesFragmento.mDatosMeteoList.getdatosMeteoList().get(itemSelect2);
//
//		AlertDialog.Builder builder2 = new AlertDialog.Builder(this)
//		.setTitle(aTituloString)
////		.setTitle(R.string.borrar_localidad_titulo)
//		.setMessage(aMsgString)
////				.setMessage(datosMeteoSelect2.getNomLocalidad()
////						+ "\n" + datosMeteoSelect2.getNomPaisCiudad())
//				.setPositiveButton(R.string.accept,
//						new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface arg0, int arg1) {
//						eliminarItem();
//					}
//				});
//		builder2.show();
//
//	}

}
