package com.jmfierro.utad.meteo.SQL;

import java.util.ArrayList;
import java.util.List;

import com.jmfierro.utad.meteo.Actividades.MainActivity;
import com.jmfierro.utad.meteo.Datos.Nota;
import com.jmfierro.utad.meteo.SQL.MySQLiteOpenHelper.TablaNotas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ListView;

public class NotasDataSource {
	private SQLiteDatabase db;
	private MySQLiteOpenHelper dbHelper;
	private String[] columnas = { TablaNotas.COLUMNA_ID,
			TablaNotas.COLUMNA_TEXTO };

	public NotasDataSource(Context context) {
		dbHelper = MySQLiteOpenHelper.getInstance(context);
	}

	public void open() {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void crearNota(String nota) {
		ContentValues values = new ContentValues();
		values.put(TablaNotas.COLUMNA_TEXTO, nota);
		db.insert(TablaNotas.TABLA_NOTAS, null, values);
	}

	public void replaceNota(Nota nota) {
		ContentValues values = new ContentValues();
		values.put(TablaNotas.COLUMNA_TEXTO, nota.gettexto());
		long id = nota.getId();

		//		db.delete(TablaNotas.TABLA_NOTAS, TablaNotas.COLUMNA_ID + " = " + id,
//				null);
//		db.replace(TablaNotas.TABLA_NOTAS, TablaNotas.COLUMNA_ID + " = " + id,
//				values);
		db.update(TablaNotas.TABLA_NOTAS, 
				values, 
				TablaNotas.COLUMNA_ID + " = " + id, null);
		
		
//		// Representa la cláusula WHERE
//		String selection = BlogDbContract.PostEntry._ID + " = ?";
//		String[] selectionArgs = new String[] { ""+post.getId() };
//
//		int count = mDb.update(
//				BlogDbContract.PostEntry.TABLE_NAME,
//				values,
//				selection,
//				selectionArgs);
//		
//		return count;

		
	
		
//		ContentValues values = new ContentValues();
//		values.put(TablaNotas.COLUMNA_TEXTO, nota);
//		db.insert(TablaNotas.TABLA_NOTAS, null, values);
//
//		db.replace(TablaNotas.TABLA_NOTAS, null, values);
	}


	public List<Nota> getAllNotas() {
		List<Nota> listaNotas = new ArrayList<Nota>();

		Cursor cursor = db.query(TablaNotas.TABLA_NOTAS, columnas, null, null,
				null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Nota nuevaNota = cursorToNota(cursor);
			listaNotas.add(nuevaNota);
			cursor.moveToNext();
		}

		cursor.close();
		return listaNotas;
	}

	
	static public void borrarAllNotasOpenClose() {
		
		ListView lvNotas;
		NotasDataSource dataSource;
		
		dataSource = new NotasDataSource(MainActivity.mMainActivityThis); //this);
//		dataSource = new NotasDataSource(Config.mContextMain); //this);
		dataSource.open();
		List<Nota> listaNotas = dataSource.getAllNotas(); 

		for(int i = 0; i < listaNotas.size(); i++) { 
			Nota nota = listaNotas.get(i);
			dataSource.borrarNota(nota);
		}
		
		dataSource.close();
	}
	
	
	static public List<Nota> getAllNotasOpenClose() {

			
		NotasDataSource dataSource;
		
		dataSource = new NotasDataSource(MainActivity.mMainActivityThis); //this);
//		dataSource = new NotasDataSource(Config.mContextMain); //this);
		dataSource.open();
		List<Nota> listaNotas = dataSource.getAllNotas(); 
		
		dataSource.close();
		
		return listaNotas;
	}
	
	public void borrarNota(Nota nota) {
		long id = nota.getId();
		db.delete(TablaNotas.TABLA_NOTAS, TablaNotas.COLUMNA_ID + " = " + id,
				null);
	}

	private Nota cursorToNota(Cursor cursor) {
		Nota nota = new Nota();
		nota.setId(cursor.getLong(0));
		nota.setTexto(cursor.getString(1));
		return nota;
	}
	
	public Nota getNotas(long id) {
		List<Nota> listaNotas = new ArrayList<Nota>();

		Cursor cursor = db.query(TablaNotas.TABLA_NOTAS, columnas, null, null,
				null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Nota nuevaNota = cursorToNota(cursor);
			if (nuevaNota.getId()==id)
				return nuevaNota;
//			listaNotas.add(nuevaNota);
			cursor.moveToNext();
		}

		cursor.close();
		return null; //listaNotas;
	}

	public Nota getNotas(String stringNota) {
		List<Nota> listaNotas = new ArrayList<Nota>();

		Cursor cursor = db.query(TablaNotas.TABLA_NOTAS, columnas, null, null,
				null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Nota nuevaNota = cursorToNota(cursor);
			if (nuevaNota.gettexto().equals(stringNota))
				return nuevaNota;
//			listaNotas.add(nuevaNota);
			cursor.moveToNext();
		}

		cursor.close();
		return null; //listaNotas;
	}

}
